<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until page content tag
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <!--[if lte IE 11]>
    <script >window.location = '/update-browser';</script>
    <![endif]-->
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <?php include_once( 'inc/env-cfg.php' ); ?>
    <?php include_once( 'inc/js-ns.php' ); ?>
    <?php wp_head(); ?>
</head>

<?php
    global $post;
    $post_slug=$post->post_name;
?>

<body <?php body_class(); ?> data-ga-category="<?php echo $post_slug.'-page'; ?>">

    <?php if ( is_page( 'realestate' ) ) :

        get_template_part( 'template-parts/header/header', 'realestate' );

    else : ?>

        <header class='header' data-ga-action="header">
            <div class='header__content'>

                <?php get_template_part( 'template-parts/header/header', 'logo' ); ?>

                <?php
                    if ( has_nav_menu( 'header-nav' ) ) :
                        get_template_part( 'template-parts/navigation/navigation', 'header' );
                    endif;
                ?>

                <div class="header__menu" data-ga-label="menu">
                    <div></div>
                    <div></div>
                </div>
            </div>
        </header>

    <?php endif; ?>

    <article>