<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */

get_header(); ?>

    <section class="press" data-ga-category="press">
        <div class="section-content">
            <div class="title-content">
                <h1><?php single_post_title(); ?></h1>
            </div>
            <?php if ( have_posts() ) :

                /* Start the Loop */
                while ( have_posts() ) : the_post();

                    /*
                     * Include the Post-Format-specific template for the content.
                     * If you want to override this in a child theme, then include a file
                     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                     */
                    get_template_part( 'template-parts/post/content', get_post_format() );

                // End the loop.
                endwhile;

                // Numeric page navigation.
                echo paginate_links( array(
                    'prev_next' => false,
                    'type'      => 'list'
                ) );

            // If no content, include the "No posts found" template.
            else :

                get_template_part( 'template-parts/post/content', 'none' );

            endif; ?>
        </div>
    </section>

<?php get_footer();
