<?php
/**
 * The template for displaying all single blog posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 */

get_header(); ?>

    <section class="blog-single" data-ga-category="blog">
        <div class="section-content">

        <?php
            /* Start the loop. */
            while ( have_posts() ) : the_post();

                get_template_part( 'template-parts/blog/single/content', get_post_format() );

            // End the loop.
            endwhile;
        ?>

        </div>
    </section>

<?php get_footer();
