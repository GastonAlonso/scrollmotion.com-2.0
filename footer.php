<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the page content tag all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?>

    </article>

    <?php
        // Add modal template for Real Estate page
        if ( is_page( 'realestate' ) ) :

            get_template_part( '/template-parts/components/modal', 'realestate' );

        endif;
    ?>

    <footer class='footer' data-ga-action="footer">
        <div class='footer__content'>
            <?php
                get_template_part( 'template-parts/footer/footer', 'logos' );

                get_template_part( 'template-parts/footer/footer', 'social' );

                if ( has_nav_menu( 'header-nav' ) ) :

                    get_template_part( 'template-parts/navigation/navigation', 'footer' );

                endif;

                get_template_part( 'template-parts/footer/footer', 'legal' );

                // video scrim
                get_template_part( '/template-parts/components/video-scrim' );
            ?>
        </div>
    </footer>

    <?php wp_footer(); ?>

</body>
</html>
