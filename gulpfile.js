var gulp         = require('gulp');
var plumber      = require('gulp-plumber');
var sourcemaps   = require('gulp-sourcemaps');
var less         = require('gulp-less');
var gutil        = require('gulp-util');
var autoprefixer = require('gulp-autoprefixer');
var filter       = require('gulp-filter');
var rename       = require('gulp-rename');
var minifycss    = require('gulp-uglifycss');
var runSequence  = require('gulp-run-sequence');
var concat       = require('gulp-concat');
var uglify       = require('gulp-uglify');

gulp.task('styles', function () {
    return gulp.src([
            './assets/less/style.less',
            './assets/less/style-legacy.less'
        ]) // entry points
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(less().on('error', function (err) {
            gutil.log(err);
            this.emit('end')
        }))
        .pipe(sourcemaps.write({ includeContent: false }))
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(autoprefixer('last 2 version', '> 1%', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(sourcemaps.write('.'))
        .pipe(plumber.stop())
        .pipe(gulp.dest('./'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(minifycss({
            maxLineLen: 80
        }))
        .pipe(gulp.dest('./'));
});

// Concatenate and uglify javascript for production.
gulp.task('scripts', function () {
    return gulp.src('./assets/js/**/*.js')
        .pipe(concat('scripts.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./'));
});

gulp.task('build', function (cb) {
    runSequence('styles');
    runSequence('scripts');
});

gulp.task('default', ['styles', 'scripts'], function () {
    gulp.watch('./assets/less/**/*.less', ['styles']);
    gulp.watch('./assets/js/**/*.js', ['scripts']);
});
