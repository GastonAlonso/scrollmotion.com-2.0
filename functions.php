<?php
/**
 * Ingage theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function ingage_theme_setup() {

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
     */
    add_theme_support( 'post-thumbnails' );

    // register custom post type 'Blog'.
    function create_blog_post_type() {
        register_post_type( 'blog-posts',
              array(
                  'public' => true,
                  'labels' => array(
                      'name'      => __( 'Blog Posts' ),
                      'all_items' => __( 'All Posts' )
                  ),
                  'supports' => array(
                      'title',
                      'editor',
                      'author',
                      'thumbnail',
                      'comments',
                  )
              )
        );
    }
    add_action( 'init', 'create_blog_post_type' );

    // add support for 'Blogs' as a new post format.
    add_post_type_support( 'blog-posts', 'post-formats' );

    // Add category and tags for 'Blog' posts.
    add_action( 'init', 'register_taxonomies_for_blog' );
    function register_taxonomies_for_blog() {
        register_taxonomy_for_object_type( 'category', 'blog-posts');
        register_taxonomy_for_object_type( 'post_tag', 'blog-posts');
    }

    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus( array(
        'header-nav'         => 'Header Navigation',
        'header-nav-classic' => 'Alternate Header Navigation',
        'footer-nav'         => 'Footer Navigation'
    ) );
}
add_action( 'after_setup_theme', 'ingage_theme_setup' );

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ...
 */
function ingage_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'ingage_excerpt_more' );

function ingage_scripts() {
    // Remove out-of-the-box jQuery
    wp_deregister_script('jquery');

    // Register jQuery CDN of choice
    wp_register_script('jquery', 'https://code.jquery.com/jquery-2.1.1.min.js' , false, '2.1.1');

    // Register Stripe
    wp_register_script('stripe', 'https://js.stripe.com/v2/');

    // Register Ingage SDK
    wp_register_script('ingage', 'https://s3.amazonaws.com/ingage-sdk-prod/ingage-sdk.js', null, true);

    // Only add Stripe to Real Estate Dashboard
    if ( is_page ( 'real-estate-dashboard' ) ) {

        return wp_enqueue_script( 'stripe' );
    }

    // On legacy pages load legacy style.
    if ( is_404()    ||
         is_home()   ||
         is_single() ||
         is_page( array(
             'support',
             'about',
             'press',
             'blog',
             'careers',
             'privacy',
             'terms'
         ) ) ) {

        wp_enqueue_style( 'theme-style', get_template_directory_uri() . '/style-legacy.min.css' );
    }

    // Load our main stylesheet on all other pages.
    else {
        wp_enqueue_style( 'theme-style', get_template_directory_uri() . '/style.min.css' );
    }

    // Enqueue jQuery.
    wp_enqueue_script('jquery');

    // Enqueue Stripe
    wp_enqueue_script( 'stripe' );

    // Enqueue scripts
    wp_enqueue_script('scripts.js', get_template_directory_uri() . '/scripts.js');
}

add_action( 'wp_enqueue_scripts', 'ingage_scripts' );

function addDataAttr( $items, $args ) {
    $dom = new DOMDocument();
    $dom->loadHTML($items);
    $find = $dom->getElementsByTagName('a');

    foreach ($find as $item ) :
        $attr_value = str_replace(' ', '-', strtolower($item->textContent)).'-link';
        $item->setAttribute('data-ga-label', $attr_value);
    endforeach;

    return $dom->saveHTML();
}

add_filter('wp_nav_menu_items', 'addDataAttr', 10, 2);

// Function for loading images smarter
if (!function_exists('checkIfFieldValueIsIDAndReturnTheURLAndDeleteThisFunctionLaterLikeReallyDoIt')) {
    function checkIfFieldValueIsIDAndReturnTheURLAndDeleteThisFunctionLaterLikeReallyDoIt ($data, $size) {
        if (is_numeric($data)) {
            $image = wp_get_attachment_image_src($data, $size);

            echo $image[0];
        } else {
            echo $data;
        }
    }
}
