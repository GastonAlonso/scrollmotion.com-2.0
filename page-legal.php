<?php ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#"><!--<![endif]-->
<head profile="http://www.w3.org/1999/xhtml/vocab">
    <title>Master Subscription Agreement | ScrollMotion</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow,noodp">
    <meta name="slurp" content="noydir">
    <meta name="msnbot" content="noodp">
    <meta name="googlebot" content="noodp">
    <meta name="generator" content="www.scrollmotion.com">
    <meta name="language" content="en-us">
    <meta http-equiv="content-language" content="en-us">
    <link type="text/css" rel="stylesheet" href="//fast.fonts.net/cssapi/1c5badec-e00c-4931-a842-66f976e440d7.css" media="all">
    <link rel="shortcut icon" type="image/x-icon"  href="<?php echo get_site_icon_url(); ?>">
    <link rel="SHORTCUT ICON" type="image/vnd.microsoft.icon" href="<?php echo get_site_icon_url(); ?>">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/style-legal.css" />
</head>
    <body class="html guest page-legal wide">
        <?php
            if ( have_posts() ) : while ( have_posts() ) : the_post();
                the_content();
            endwhile;  endif;
        ?>
    </body>
</html>
<?php ?>










