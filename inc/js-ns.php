<script>
    // generate namespace
    window.scrollmotion = window.scrollmotion || {};

    // store variables
    window.scrollmotion.homeUrl         = '<?php echo home_url(); ?>';
    window.scrollmotion.directoryUri    = '<?php echo get_template_directory_uri(); ?>';
    window.scrollmotion.apiUrl          = '<?php echo $sm_config['api_url'] ?>';
    window.scrollmotion.clientId        = '<?php echo $sm_config['client_id'] ?>';
    window.scrollmotion.stripePublicKey = '<?php echo $sm_config['stripe_public_key'] ?>';
</script>