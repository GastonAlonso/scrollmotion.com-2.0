<!DOCTYPE html>
<html>
<head>
    <title>scrollmotion.com</title>
    <link rel="shortcut icon" href="<?php echo get_site_icon_url() ?>" />
    <style>
        body {
            font-family: HelveticaNeue, Helvetica, sans-serif;
            text-align:  center;
            background:  #eeeeeb;
            position:    absolute;
            margin:      0;
            bottom:      0;
            right:       0;
            color:       #4f4e5c;
            left:        0;
            top:         0;
        }
        div.header {
            background: #393842;
            margin:     auto;
            height:     64px;
            right:      0;
            width:      100%;
            left:       0;
            top:        0;
        }
        img {
            margin-top: 5px;
            height:     53px;
        }
        h1 {
            line-height: 48px;
            margin-top:  -48px;
            position:    relative;
            top:         40%;
        }
        a {
            color: #2ea1e8;
        }
    </style>
</head>
<body>
    <div class="header">
        <img src="<?php bloginfo('template_url'); ?>/assets/images/icon-scrollmotion.png">
    </div>
    <h1>
        Your Browser is no longer supported.<br>
        Please update to a <a href="http://outdatedbrowser.com/en">modern browser</a>.
    </h1>
</body>
</html>