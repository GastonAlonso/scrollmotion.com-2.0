<?php

switch ($_GET['page']) {
    case 'welcome':
        include('template-parts/beta/welcome.php');
        break;

    case 'optout':
        include_once('template-parts/beta/opt-out.php');
        break;

    default:
        include_once('template-parts/beta/signup.php');
        break;
}

?>
