<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */

get_header(); ?>

<?php

if ( have_rows( 'content_blocks' ) ) :

    while ( have_rows( 'content_blocks' ) ) : the_row();

        get_template_part( 'template-parts/content-blocks-legacy/block', get_row_layout() );

    endwhile;

elseif ( have_rows( 'content_blocks_2' ) ) :

    while ( have_rows( 'content_blocks_2' ) ) : the_row();

        get_template_part( 'template-parts/content-blocks/block', get_row_layout() );

    endwhile;

else :

    while ( have_posts() ) : the_post();

        get_template_part( 'template-parts/page/content', 'page' );

    endwhile; // End of the loop.

endif;

?>


<?php get_footer();
