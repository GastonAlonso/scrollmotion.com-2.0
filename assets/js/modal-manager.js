(function () {
    function Manager (config) {
        this.init(config);

        return this;
    }

    Manager.prototype = {
        init: function (config) {
            this.bindMethods();

            this.registrationModal = new window.scrollmotion.Registration({
                modal: config.registrationElement,
                session: this
            });

            this.loginModal = new window.scrollmotion.Login({
                modal: config.loginElement,
                session: this
            });

            this.purchaseModal = new window.scrollmotion.Purchase({
                modal: config.purchaseElement,
                session: this
            });

            this.messageModal = new window.scrollmotion.Message({
                modal: config.messageElement,
                session: this
            });

            this.subscriptionModal = new window.scrollmotion.Subscription({
                modal: config.subscriptionElement,
                session: this
            });

            this.userData = {
                username:  '',
                password:  '',
                firstName: '',
                lastName:  '',
                userId:    '',
                email:     '',
                token:     '',
                tier:      '',
                coupon:    '',
                address: {
                    name:    '',
                    line1:   '',
                    line2:   '',
                    city:    '',
                    state:   '',
                    zip:     '',
                    country: ''
                }
            };
        },

        bindMethods: function () {
            this.hideModals = this.hideModals.bind(this);
            this.register   = this.register.bind(this);
            this.login      = this.login.bind(this);
            this.purchase   = this.purchase.bind(this);
        },

        setUserData: function (data) {
            this.userData = $.extend(true, {}, this.userData, data);
        },

        getUserData: function () {
            return $.extend(true, {}, this.userData);
        },

        hideModals: function () {
            this.registrationModal.hide();
            this.loginModal.hide();
            this.purchaseModal.hide();
            this.messageModal.hide();
        },

        clear: function () {
            this.clearUserData();
            this.clearModals();
        },

        clearUserData: function () {
            this.userData = {
                username:  '',
                password:  '',
                firstName: '',
                lastName:  '',
                userId:    '',
                email:     '',
                token:     '',
                tier:      '',
                coupon:    '',
                address: {
                    name:    '',
                    line1:   '',
                    line2:   '',
                    city:    '',
                    state:   '',
                    zip:     '',
                    country: ''
                }
            };
        },

        clearModals: function () {

        },

        register: function (data) {
            this.clearUserData();
            this.setUserData(data);
            this.hideModals();
            this.registrationModal.show();
        },

        login: function (data) {
            this.hideModals();

            // default to monthly if free user
            // has account and is upgrading
            if (this.userData.tier === 'free') {
                this.setUserData({ tier: 'web_paid_monthly' });
            }

            this.loginModal.login(data);
        },

        purchase: function (data) {
            this.hideModals();
            this.purchaseModal.show(data);
        },

        message: function (messageType) {
            this.hideModals();
            this.messageModal.show(messageType);
        },

        subscriptions: function () {
            this.hideModals();
            this.subscriptionModal.show();
        }
    }

    window.scrollmotion.Manager = Manager;
})();
