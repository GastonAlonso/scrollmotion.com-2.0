$(document).ready(function () {
    var $getStartedButtons = $('.realestate-getstarted');
    var $getStartedModal   = $('.realestate-modal');

    $getStartedButtons.on('click', function (evt) {
        evt.preventDefault();

        if ($getStartedModal.length > 0) {
            $getStartedModal.find('.close-button').on('click', function () {
                $getStartedModal.hide();
            });

            $getStartedModal.show();
        }
    });
});
