$(document).ready(function () {
    var $videos = $('.video-focus');

    function checkFocusedVideo () {
        // Get current browser top and bottom
        var scrollTop    = $(window).scrollTop();
        var scrollBottom = $(window).scrollTop() + $(window).height();

        // how many pixels of each video are onscreen
        var videoOnScreenHeights = [];

        for (i = 0; i < $videos.length; i++) {
            var $video = $($videos[i]);

            var yTopVideo    = $video.offset().top;
            var yBottomVideo = $video.height() + yTopVideo;

            // onscreen
            if(scrollTop < yBottomVideo && scrollBottom > yTopVideo){
                // top above screen
                if (scrollTop > yTopVideo) {
                    videoOnScreenHeights.push(yBottomVideo - scrollTop);
                }

                // bottom below screen
                else if (yBottomVideo > scrollBottom) {
                    videoOnScreenHeights.push(scrollBottom - yTopVideo);
                }

                // fully onscreen
                else {
                    videoOnScreenHeights.push($video.height());
                }
            } 

            // offscreen
            else {
                videoOnScreenHeights.push(0)
           }
        }

        playCorrectVideo(videoOnScreenHeights);
    }

    function playCorrectVideo(videoOnScreenHeights) {
        var max = videoOnScreenHeights.reduce(function(a, b) {
            return Math.max(a, b);
        });

        $videos[videoOnScreenHeights.indexOf(max)].play();
    }

    if ($videos.length > 0) {
        $(document).on('scroll', checkFocusedVideo);
    }
});
