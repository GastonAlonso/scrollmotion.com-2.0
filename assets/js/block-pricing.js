$(document).ready(function () {
    var registrationButtons    = $('.new-registration');
    var aboutSubscriptionsLink = $('.about-subscriptions');

    var session = new window.scrollmotion.Manager({
        registrationElement: $('#register-modal'),
        loginElement:        $('#login-modal'),
        purchaseElement:     $('#purchase-modal'),
        messageElement:      $('#message-modal'),
        subscriptionElement: $('#subscriptions-modal')
    });

    registrationButtons.on('click', function (e) {
        var targetTier = $(e.target).attr('data-tier');
        session.register({ tier: targetTier });
        return false;
    });

    aboutSubscriptionsLink.on('click', function () {
        session.subscriptions();
        return false;
    });
});