$(document).ready(function() {
    var $topVideoContainer = $('.top-video-container');

    $topVideoContainer.on('click', '.top-video-play-button', function () {
        var $parent        = $(this).parent();
        var $topVideoPlay  = $parent.find('.top-video-play-button');
        var $topVideoStill = $parent.find('.top-video-still');
        var $topVideo      = $parent.find('.top-video');

        // hide play button and still
        $topVideoPlay.hide();
        $topVideoStill.hide();

        // show and play video
        $topVideo.show();
        $topVideo.get(0).play();
    });
});
