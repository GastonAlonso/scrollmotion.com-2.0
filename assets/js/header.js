$(document).ready(function() {
    var $header = $('.header, .realestate-header');
    var $menu   = $('.header__menu');

    if ($(window).scrollTop() === 0) {
        $(window).scroll(function () {
            $header.addClass('header--dark');
        });
    }

    else {
        $header.addClass('header--dark');
    }

    // prevents an event's default behavior
    function preventDefault (event) {
        event.preventDefault();
    }

    $menu.on('click', function () {
        // not open yet
        if (!$header.hasClass('header--open')) {
            $header.addClass('header--open');
            $header.addClass('header--dark');

            // stop user from being able to scroll background
            document.body.addEventListener('touchmove', preventDefault, false);
            $(document.body).css('overflow', 'hidden');
        }

        // already open
        else {
            $header.removeClass('header--open');

            // let the user scroll the page again
            document.body.removeEventListener('touchmove', preventDefault, false);
            $(document.body).css('overflow', 'initial');
        }
    });
});
