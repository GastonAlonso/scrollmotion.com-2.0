$(document).ready(function() {
    var $soundToggles = $('.mutable-toggle');

    $soundToggles.on('click', function (evt) {
        var $videos      = $soundToggles.parent().find('.mutable-video');
        var $parentVideo = $(this).parent().find('.mutable-video');

        // mute all other videos
        $videos.not(this).prop('muted', true); 
        $soundToggles.not(this).addClass('muted');

        if ($(this).hasClass('muted')) {
            $(this).removeClass('muted');
            $parentVideo.prop('muted', false);
        }

        else {
            $(this).addClass('muted');
            $parentVideo.prop('muted', true);
        }
    });
});
