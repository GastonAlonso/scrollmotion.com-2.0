(function () {
    function isValidEmail (email) {
        var emailRegEx = /(.+)@(.+){2,}\.(.+){2,}/;

        if( emailRegEx.test(email) ) {
            return true;
        }

        return false;
    }

    function isValidPassword (pass) {
        if (typeof pass !== 'string') {
            return false;
        }

        if (pass.length < 8) {
            return false;
        }

        if (pass.length > 30) {
            return false;
        }

        if (!pass.match(/(?=.*[a-z])/)) {
            return false;
        }

        if (!pass.match(/(?=.*[A-Z])/)) {
            return false;
        }

        return true;
    }

    function Registration (data) {
        this.init(data);
        return this;
    }

    Registration.prototype = {
        init: function (data) {
            this.$modal  = data.modal;
            this.$submit = this.$modal.find('input[type="submit"]');
            this.$error  = this.$modal.find('.error');
            this.session = data.session;

            this.$submit.on('click', this.handleClick.bind(this));
            this.$modal.find('.sign-in-button').on('click', this.session.login.bind(this));
            this.$modal.find('.close').on('click', this.hide.bind(this));
        },

        handleClick: function () {
            this.clearErrors();
            this.disableForm();

            this.register({
                first_name:      this.$modal.find('#first-name').val(),
                last_name:       this.$modal.find('#last-name').val(),
                username:        this.$modal.find('#email').val(),
                password:        this.$modal.find('#password').val(),
                confirmPassword: this.$modal.find('#confirm-password').val()
            });

            return false;
        },

        show: function () {
            this.clearForm();
            this.enableForm();
            this.$modal.removeClass('hidden');
        },

        hide: function () {
            this.clearForm();
            this.$modal.addClass('hidden');
        },

        register: function (registrationData) {
            if (this.isDataValid(registrationData)) {
                this.session.setUserData({
                    username: registrationData.username,
                    password: registrationData.password
                });
                this._send(registrationData);
            }

            else {
                this.enableForm();
            }
        },

        _send: function (data) {
            $.ajax({
                url:         window.scrollmotion.apiUrl + '/sign_up/',
                type:        'POST',
                data:        data,
                crossDomain: true,
                headers: {
                    'X-CLIENT-ID': window.scrollmotion.clientId
                },
                success: this.handleSuccess.bind(this),
                error:   this.handleError.bind(this)
            });
        },

        handleSuccess: function (data, status, response) {
            mixpanel.identify(data.id);
            mixpanel.alias(data.username);

            mixpanel.people.set({
                "$firstName":  data.first_name,
                "$lastName":   data.last_name,
                "$name":       data.first_name + ' ' + data.last_name,
                "$created":    new Date(),
                "$email":      data.username,
                userId:        data.id,
                accountId:     data.id,
                accountName:   data.username
            });

            var eventName = 'convert-reg-from-web';

            if (window.location.href.match('realestate')) {
                eventName = 'convert-reg-from-real-estate';
            }

            mixpanel.track(eventName, {
                eventCategory: 'scrollmotion.com'
            });

            this.session.setUserData({
                userId:    data.id,
                firstName: data.first_name,
                lastName:  data.last_name,
                name:      data.first_name + ' ' + data.last_name,
                email:     data.username,
                username:  data.username
            });

            var userData = this.session.getUserData();
            var tier = userData.tier;

            if (userData.tier === 'web_paid_monthly' || userData.tier === 'web_paid_yearly') {
                this.session.login();
            }

            else {
                this.session.message('welcome');
            }
        },

        handleError: function (data, status, error) {
            var response = JSON.parse(data.responseText);
            var errorMessage;

            this.session.setUserData({
                password: ''
            });

            if (response.errors[0].code === 'already_exists') {
                errorMessage = 'User already exists.';
            }

            else if (response.errors[0].code === 'invalid') {
                errorMessage = 'Invalid password.';
            }

            this.enableForm();
            this.error({
                message: errorMessage
            });
        },

        isDataValid: function (data) {
            var hasErrors = false;

            if (data.first_name.trim() === '') {
                this.error({
                    message: 'First name field blank.',
                    id:      '#first-name'
                });
                hasErrors = true;
            }

            if (data.last_name.trim() === '') {
                this.error({
                    message: 'Last name field blank.',
                    id:      '#last-name'
                });
                hasErrors = true;
            }

            if (!isValidEmail(data.username)) {
                this.error({
                    message: 'Not a valid email address.',
                    id:      '#email'
                });
                hasErrors = true;
            }

            if (!isValidPassword(data.password)) {
                this.error({
                    message: 'Invalid password.',
                    id:      '#password'
                });
                hasErrors = true;
            }

            if (data.password !== data.confirmPassword) {
                this.error({
                    message: 'Passwords do not match.',
                    id:      '#confirm-password'
                });
                hasErrors = true;
            }

            return hasErrors === false;
        },

        error: function (error) {
            this.$error.append($('<div>').html(error.message));
            this.$modal.find(error.id).addClass('error');
        },

        clearErrors: function () {
            this.$modal.find('input').each(function (index) {
                if ($(this).attr('type') !== 'submit') {
                    $(this).removeClass('error');
                }
            });

            this.$error.html('');
        },

        clearForm: function () {
            this.clearErrors();

            this.$modal.find('input').each(function (index) {
                if ($(this).attr('type') !== 'submit') {
                    $(this).val('');
                }
            });
        },

        disableForm: function () {
            this.$modal.find('input').each(function (index) {
                $(this).attr('disabled', 'disabled');
            });
        },

        enableForm: function () {
            this.$modal.find('input').each(function (index) {
                $(this).attr('disabled', false);
            });
        }
    };

    window.scrollmotion.Registration = Registration;
})();