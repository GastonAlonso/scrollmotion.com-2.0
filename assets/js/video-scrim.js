$(document).ready(function() {
    var $videoPlay = $('.video-play-button');

    $videoPlay.on('click', function () {
        var $scrim = $('.video__scrim');
        var $video = $scrim.find('.scrim__video');
        var $close = $scrim.find('.scrim__close');

        $scrim.addClass('video__scrim--open');
        $video.attr('src', $(this).data('video'));
        $video.get(0).play();

        $close.on('click', function () {
            $scrim.removeClass('video__scrim--open');
            $video.get(0).pause();
        });
    });
});