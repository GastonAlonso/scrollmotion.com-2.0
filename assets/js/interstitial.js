var INTERSTITIAL_PATH = 'ingage-showcase';
var IS_IPAD = navigator.userAgent.match(/iPad/i) != null;
var IS_MOBILE = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

$(function () {
    var path = document.location.pathname.split('/').filter(Boolean);

    if (path[0] === INTERSTITIAL_PATH) {
        $('a').on('click', function (event) {
            var $link = $(this);

            var href = $link.attr('href');

            if (href.startsWith('https://ingage.scrollmotion.com') ||
                href.startsWith('http://ingage.scrollmotion.com')) {

                var url = new URL(href);

                var path = url.pathname.split('/').filter(Boolean);
                var bundle = path[0].split('?')[0];

                // Open either app or web experience
                if (IS_IPAD) {
                    // window.open(href);
                }

                else if (IS_MOBILE) {
                    // window.open(href);
                }

                // Open install guide
                else {
                    event.preventDefault();
                    window.open('/install-guide');
                }
            }
        });
    }
});
