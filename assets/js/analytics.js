;(function () {
    window.Analytics = {
        videoStage: {},

        init: function () {
            $('[data-ga-category]').on(
                'mousedown',
                ':not(video)[data-ga-label]',
                this.listenUserAction.bind(this)
            );

            $('video[controls][data-ga-label]').on(
                'play playing progress ended',
                this.listenVideoProgress.bind(this)
            );
        },

        listenVideoProgress: function (e) {
            var video;
            var progress;
            var stage;
            var updateProgress;

            video    = e.currentTarget;
            progress = video.currentTime / video.duration;
            stage    = this.videoStage;

            updateProgress = function (progress) {
                $(video).data('ga-label', progress);
                this.listenUserAction(e);
            }.bind(this);

            if (e.type === 'play' || e.type === 'playing') {
                stage = {};

                if (progress > 0)    stage[0] = true;
                if (progress > 0.25) stage[1] = true;
                if (progress > 0.50) stage[2] = true;
                if (progress > 0.75) stage[3] = true;

                this.videoStage = stage;
            }

            if (!stage[0] && progress === 0 && e.type === 'playing') {
                updateProgress('0%');
                stage[0] = true;
            } else if (!stage[1] && progress > 0.25) {
                updateProgress('25%');
                stage[1] = true;
            } else if (!stage[2] && progress > 0.50) {
                updateProgress('50%');
                stage[2] = true;
            } else if (!stage[3] && progress > 0.75) {
                updateProgress('75%');
                stage[3] = true;
            } else if (progress === 1) {
                updateProgress('100%');
            }
        },

        listenUserAction: function (e) {
            e.stopPropagation();

            var $label;
            var $action;
            var $category;
            var label;
            var action;
            var category;

            $label = $(e.currentTarget);

            label = $label.data('ga-label') || $label.closest('[data-ga-label]').data('ga-label');

            $action = $label.closest('[data-ga-action]');

            if ($action.length) {
                action = $action.data('ga-action');
            }

            $category = $label.closest('[data-ga-category]');

            if ($category.length) {
                category = $category.data('ga-category');
            }

            if (!category) {
                category = null;
            }

            if (!action) {
                action = null;
            }

            if (!label) {
                label = null;
            }

            else if (typeof label == 'number') {
                label = label.toString();
            }

            this.trackEvent(category, action, label);
        },

        trackEvent: function (category, action, label, opt_value, opt_no_bounce) {
            var value;
            var bounce;

            value  = opt_value || null ;
            bounce = opt_no_bounce || false;

            if (window.ga) {
                window.ga('send', {
                    'hitType': 'event',
                    'eventCategory': category,
                    'eventAction': action,
                    'eventLabel': label,
                    'eventValue': value,
                    'nonInteraction': bounce,
                    'location': document.location.href
                });
            }

            var eventName;

            if (window.mixpanel) {
                eventName = [category, action, label].join('-').replace(/\s/g, '-');

                window.mixpanel.track(eventName, {
                    eventCategory:  'scrollmotion.com',
                    category:       category,
                    action:         action,
                    label:          label,
                    value:          value,
                    nonInteraction: bounce,
                    location:       document.location.href
                });
            }
        }
    };

    $(Analytics.init.bind(Analytics));
})();
