(function () {
    function Message (config) {
        this.init(config);

        return this;
    }

    Message.prototype = {
        init: function (config) {
            this.$modal  = config.modal;
            this.session = config.session;

            this.$modal.find('.close').on('click', this.hide.bind(this));
            this.$modal.find('.upgrade-link').on('click', function () {
                config.session.login();

                return false;
            });
        },

        show: function (messageType) {
            this.updateMessage(messageType);

            this.$modal.removeClass('hidden');
        },

        hide: function () {
            this.$modal.addClass('hidden');
        },

        updateMessage: function (messageType) {
            var userData = this.session.getUserData();

            var messageData = this.getMessageData(messageType);

            if (messageType === 'success') {
                this.hideLink();
            }

            else {
                this.showLink();
            }

            this.setTitle(messageData.title);
            this.setMessage(messageData.message);
            this.setEmail(userData.username);
        },

        getMessageData: function (messageType) {
            switch (messageType) {
                case 'welcome':
                    return {
                        title:   'Welcome!',
                        message: 'You’ve successfully registered with Ingage for iPad.'
                    };
                case 'success':
                    return {
                        title:   'Success!',
                        message: 'You’ve successfully upgraded to Ingage Plus.'
                    };
            }
        },

        setTitle: function (title) {
            this.$modal.find('h1').html(title);
        },

        setMessage: function (message) {
            this.$modal.find('.message').html(message);
        },

        setEmail: function (email) {
            this.$modal.find('.email').html(email);
        },

        showLink: function () {
            this.$modal.find('.upgrade-link').show();
        },

        hideLink: function () {
            this.$modal.find('.upgrade-link').hide();
        }
    };

    window.scrollmotion.Message = Message;
})();