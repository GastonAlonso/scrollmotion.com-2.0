$(document).ready(function() {
    var $container = $('html, body');

    $('.page-scroller').each(function (i, el) {
        $(el).on('click', scrollDown);
    });

    function scrollDown (evt) {
        evt.preventDefault();

        var anchorId     = '#' + $(this).data('anchor-id');
        var $anchor      = $(anchorId);
        var headerHeight = 64;

        if ($anchor.length) {
            $container.animate({
                scrollTop: $anchor.offset().top - headerHeight
            }, 500);
        }
    }
});
