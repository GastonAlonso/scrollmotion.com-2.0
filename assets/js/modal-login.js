(function () {
    function isValidEmail (email) {
        var emailRegEx = /(.+)@(.+){2,}\.(.+){2,}/;

        if( emailRegEx.test(email) ) {
            return true;
        }

        return false;
    }

    function isValidPassword (pass) {
        if (typeof pass !== 'string') {
            return false;
        }

        if (pass.length < 8) {
            return false;
        }

        if (pass.length > 30) {
            return false;
        }

        if (!pass.match(/(?=.*[a-z])/)) {
            return false;
        }

        if (!pass.match(/(?=.*[A-Z])/)) {
            return false;
        }

        return true;
    }

    function Login (data) {
        this.init(data);
        return this;
    }

    Login.prototype = {
        init: function (data) {
            this.$modal  = data.modal;
            this.$content = this.$modal.find('.modal-content');
            this.session = data.session;

            this.state = {};

            this._stateListeners = [];
        },

        initElements: function () {
            this.$submit  = this.$modal.find('input[type="submit"]');
            this.$error   = this.$modal.find('.error');
        },

        initHandlers: function () {
            this.$submit.on('click', this.handleClick.bind(this));
            this.$modal.find('.new-registration').on('click', function () {
                this.setState({ mode: 'login' });
                this.session.register();
            }.bind(this));
            this.$modal.find('.forgot-password').on('click', function () {
                this.setState({ mode: 'forgot' })
            }.bind(this));
            this.$modal.find('.close').on('click', this.hide.bind(this));
        },

        clearHandlers: function () {
            this.$submit.off('click', this.handleClick.bind(this));
            this.$modal.find('.new-registration').off('click', this.session.register);
            this.$modal.find('.close').off('click', this.hide.bind(this));
        },

        render: function () {
            switch(this.state.mode) {
                case 'forgot':
                    this.renderForgot();
                    break;
                case 'reset':
                    this.renderReset();
                    break;
                case 'login':
                default:
                    this.renderLogin();
                    break;
            }
        },

        renderLogin: function () {
            this.$content.html(
                '<h1>Ready to Upgrade?</h1>' +
                '<p class="description">Ingage Plus gives you the full power of interactivity. Create unlimited stories of any length. Unlock premium templates. Prepare to be amazed.</p>' +
                '<button class="close"></button>' +
                '<a class="sign-in new-registration">Don\'t have an account? <span>Register now</span></a>' +
                '<form id="log-in-form" action="">' +
                    '<input type="email" id="username" placeholder="Email" />' +
                    '<input type="password" id="password" placeholder="Password" />' +
                    '<div class="error"></div>' +
                    '<input type="submit" class="sign-in-submit-button" value="Continue" />' +
                    '<a class="sign-in forgot-password">Forgot password?</a>' +
                '</form>'
            );

            this.initElements();
            this.initHandlers();
        },

        renderForgot: function () {
            this.$content.html(
                '<h1>Request New Password</h1>' +
                '<p class="description">Enter the email associated with your Ingage account and we will send an email with a temporary passkey.</p>' +
                '<button class="close"></button>' +
                '<a class="sign-in new-registration">Don\'t have an account? <span>Register now</span></a>' +
                '<form id="log-in-form" action="">' +
                    '<input type="email" id="username" placeholder="Email" />' +
                    '<div class="error"></div>' +
                    '<input type="submit" class="request-password-button" value="Send" />' +
                '</form>'
            );

            this.initElements();
            this.initHandlers();
        },

        renderReset: function () {
            this.$content.html(
                '<h1>Reset Password</h1>' +
                '<button class="close"></button>' +
                '<form id="log-in-form" action="">' +
                    '<input type="email" id="username" placeholder="Email" />' +
                    '<p>Enter the temporary password sent to your email account.</p>' +
                    '<input type="password" id="passkey" placeholder="Temporary Passkey" />' +
                    '<input type="password" id="password" placeholder="New Password" />' +
                    '<p>1 cap letter &nbsp;1 lower letter &nbsp;1 digit &nbsp;8 to 30 characters</p>' +
                    '<div class="error"></div>' +
                    '<input type="submit" class="sign-in-submit-button" value="Reset" />' +
                '</form>'
            );

            this.initElements();
            this.initHandlers();
        },

        setupStateListeners: function () {
            this.addStateListener(this.render);
        },

        setState: function (data) {
            this.state = $.extend(true, {}, this.state, data);

            this.render();
        },

        _triggerStateListeners: function () {
            this._stateListeners.forEach(function (callback) {
                callback(this.state);
            });
        },

        handleClick: function () {
            this.clearErrors();
            this.disableForm();

            switch(this.state.mode) {
                case 'forgot':
                    this._requestPasskey({
                        email: this.$modal.find('#username').val()
                    });
                    break;
                case 'reset':
                    this._resetPassword({
                        username: this.$modal.find('#username').val(),
                        passkey:  this.$modal.find('#passkey').val(),
                        password: this.$modal.find('#password').val()
                    });
                    break;
                case 'login':
                default:
                    this._login({
                        username: this.$modal.find('#username').val(),
                        password: this.$modal.find('#password').val()
                    });
                    break;
            }

            return false;
        },

        show: function () {
            this.$modal.removeClass('hidden');
            this.render();
        },

        hide: function () {
            this.setState({ mode: 'login' });
            this.clearForm();
            this.$modal.addClass('hidden');
        },

        login: function () {
            var userData = this.session.getUserData();
            var loginData = {
                username: userData.username,
                password: userData.password
            };

            if (this.isDataValid(loginData, false)) {
                this._send(loginData)
            }

            else {
                this.show();
            }
        },

        _login: function (loginData) {
            if (this.isDataValid(loginData)) {
                this.disableForm();
                this.session.setUserData({
                    username: loginData.username,
                    email:    loginData.username
                });
                this._send(loginData);
            }

            else {
                this.enableForm();
            }
        },

        _send: function (loginData) {
            $.ajax({
                url: window.scrollmotion.apiUrl + '/oauth2/token/',
                type: 'POST',
                data: {
                    client_id:  window.scrollmotion.clientId,
                    grant_type: 'password',
                    username:   loginData.username,
                    password:   loginData.password,
                    clear_all:  true
                },
                crossDomain: true,
                success: this.handleSuccess.bind(this),
                error:   this.handleError.bind(this)
            });
        },

        _requestPasskey: function (data) {
            if (isValidEmail(data.email)) {
                this.disableForm();

                $.ajax({
                    method: 'POST',
                    url: window.scrollmotion.apiUrl + '/password/forgot/',
                    data: {
                        email: data.email,
                    },
                    headers: {
                        'X-CLIENT-ID': window.scrollmotion.clientId
                    },
                    success: function () {
                        this.setState({ mode: 'reset' });
                    }.bind(this),
                    error: function () {
                        this.error({
                            message: 'Invalid email',
                            id:      '#username'
                        });
                        this.enableForm();
                    }.bind(this)
                });
            }

            else {
                this.error({
                    message: 'Invalid email',
                    id:      '#username'
                });
                this.enableForm();
            }
        },

        _resetPassword: function (data) {
            if (this.isDataValid(data)) {
                this.disableForm();

                $.ajax({
                    method: 'POST',
                    url: window.scrollmotion.apiUrl + '/password/reset/',
                    data: {
                        email:    data.username,
                        passkey:  data.passkey,
                        password: data.password
                    },
                    headers: {
                        'X-CLIENT-ID': window.scrollmotion.clientId
                    },
                    success: function () {
                        this.setState({ mode: 'login' });
                    }.bind(this),
                    error: function (response) {
                        this.error({
                            message: response.responseJSON.detail
                        });
                        this.enableForm();
                    }.bind(this)
                });
            }

            else {
                this.enableForm();
            }
        },

        handleSuccess: function (data, status, response) {
            this.session.setUserData({
                token:    data.access_token,
                password: ''
            });

            mixpanel.identify(data.user.id);

            this.validateUserSubscriptions();
        },

        validateUserSubscriptions: function () {
            var userData = this.session.getUserData();
            var date     = new Date();

            $.ajax({
                url: window.scrollmotion.apiUrl + '/memberships/',
                type: 'GET',
                crossDomain: true,
                headers: {
                    Authorization: 'Bearer ' + userData.token
                },
                success: this.handleValidUser.bind(this),
                error:   this.handleError.bind(this)
            });
        },

        handleValidUser: function (data, status, response) {
            var isFreeUser = data.reduce(function (total, value) {
                if (value.tier.slug !== 'free' &&
                    value.tier.slug !== 'listing_three_months' &&
                    value.tier.slug !== 'listing_yearly') {
                    return false;
                }

                else {
                    return total;
                }
            });

            if (data.length === 0) {
                this.session.purchase();
            }

            else if (isFreeUser === false) {
                // error
                this.session.purchase({ disabled: true });
            }

            else {
                this.session.purchase();
            }
        },

        handleError: function (data, status, error) {
            this.enableForm();

            this.error({
                message: error
            });
        },

        isDataValid: function (data, error) {
            var error = error === false ? false : true;
            var hasErrors = false;

            if (!data.username || data.username.trim() === '') {
                if (error) {
                    this.error({
                        message: 'Email field empty.',
                        id:      '#username'
                    });
                }
                hasErrors = true;
            }

            else if (!isValidEmail(data.username)) {
                this.error({
                    message: 'Not a valid email address.',
                    id:      '#username'
                });
                hasErrors = true;
            }

            if (data.hasOwnProperty('passkey') && data.passkey.trim() === '') {
                if (error) {
                    this.error({
                        message: 'Passkey field empty.',
                        id:      '#passkey'
                    });
                }
                hasErrors = true;
            }

            if (!data.password || data.password.trim() === '') {
                if (error) {
                    this.error({
                        message: 'Password field empty.',
                        id:      '#password'
                    });
                }
                hasErrors = true;
            }

            else if (!isValidPassword(data.password)) {
                this.error({
                    message: 'Invalid password.',
                    id:      '#password'
                });
                hasErrors = true;
            }

            return hasErrors === false;
        },

        error: function (error) {
            this.$error.append($('<div>').html(error.message));

            if (error.id) {
                this.$modal.find(error.id).addClass('error');
            }
        },

        clearErrors: function () {
            this.$modal.find('input').each(function (index) {
                if ($(this).attr('type') !== 'submit') {
                    $(this).removeClass('error');
                }
            });

            this.$error.html('');
        },

        clearForm: function () {
            this.clearErrors();

            this.$modal.find('input').each(function (index) {
                if ($(this).attr('type') !== 'submit') {
                    $(this).val('');
                }
            });
        },

        disableForm: function () {
            this.$modal.find('input').each(function (index) {
                $(this).attr('disabled', 'disabled');
            });
        },

        enableForm: function () {
            this.$modal.find('input').each(function (index) {
                $(this).attr('disabled', false);
            });
        }
    };

    window.scrollmotion.Login = Login;
})();