(function () {
    var TIER_COST = {
        web_paid_monthly: 1499,
        web_paid_yearly:  12999
    };

    // cost of the alternate tier
    var ALT_TIER_COST = {
        web_paid_monthly: 12999,
        web_paid_yearly:  1499
    };

    function Purchase (data) {
        this.init(data);
        return this;
    }

    Purchase.prototype = {
        init: function (data) {
            this.$modal  = data.modal;
            this.$content = this.$modal.find('.modal-content');
            this.session = data.session;

            this.state = {};

            // SM test key
            Stripe.setPublishableKey(window.scrollmotion.stripePublicKey);
        },

        initElements: function () {
            this.$submit = this.$modal.find('input[type="submit"]');

            if (this.$submit.length === 0) {
                this.$submit = this.$modal.find('button');
            }

            this.$error  = this.$modal.find('.error');
        },

        initHandlers: function () {
            this.$submit.on('click', this.handleClick.bind(this));
            this.$modal.find('.close').on('click', this.hide.bind(this));

            this.$modal.find('.monthly-yearly').on('click', this.togglePurchaseButton.bind(this));
        },

        setState: function (data) {
            this.state = $.extend(true, {}, this.state, data);

            this.render();
        },

        togglePurchaseButton: function (e) {
            var userData = this.session.getUserData();

            if (userData.tier === 'web_paid_monthly') {
                this.session.setUserData({ tier: 'web_paid_yearly' });
            }

            else {
                this.session.setUserData({ tier: 'web_paid_monthly' });
            }

            this.render();
        },

        show: function (config) {
            this.$modal.removeClass('hidden');
            this.render();

            this.setConfig(config);
        },

        hide: function () {
            this.setState({ mode: 'coupon' });
            this.clearForm();
            this.$modal.addClass('hidden');
        },

        render: function () {
            switch(this.state.mode) {
                case 'invoice':
                    this.renderInvoice();
                    break;
                case 'purchase':
                    this.renderPayment();
                    break;
                case 'coupon':
                default:
                    this.renderCoupon();
                    break;
            }
        },

        renderCoupon: function () {
            this.$content.html(
                '<h1>Make Ingage work even harder.</h1>' +
                '<p class="description">Upgrade to Ingage Plus. Create unlimited stories of any length. Unlock premium templates.</p>' +
                '<button data-ga-label="close-button" class="close"></button>' +
                '<form id="coupon-form" action="">' +
                    '<input type="text" id="address-name" placeholder="Name" />' +
                    '<input type="text" id="address-line1" placeholder="Address" />' +
                    '<div>' +
                        '<div class="two-thirds">' +
                            '<input type="text" id="address-line2" placeholder="Address" />' +
                        '</div>' +
                        '<div class="third">' +
                            '<input type="text" id="address-city" placeholder="City" />' +
                        '</div>' +
                    '</div>' +
                    '<div class="thirds-container">' +
                        '<div class="third">' +
                            '<input type="text" id="address-state" placeholder="State" />' +
                        '</div>' +
                        '<div class="third">' +
                            '<input type="text" id="address-zip" placeholder="Zip Code" />' +
                        '</div>' +
                        '<div class="third">' +
                            '<input type="text" id="address-country" placeholder="Country" />' +
                        '</div>' +
                    '</div>' +
                    '<input type="text" id="coupon-code" placeholder="Coupon code" />' +
                    '<div class="error"></div>' +
                    '<input type="submit" data-ga-label="submit-coupon-button" class="coupon-submit-button" value="Apply" />' +
                '</form>'
            );

            this.initElements();
            this.initHandlers();
        },

        renderInvoice: function () {
            var invoiceData = this.calculateInvoiceData();
            var userData = this.session.getUserData()

            this.$content.html(
                '<h1>Order Total</h1>' +
                '<p class="description">Here is the total, minus any discounts and plus any taxes due. It will be worth it.</p>' +
                '<button data-ga-label="close-button" class="close"></button>' +
                '<ul>' +
                    '<li>' +
                        '<span class="product">Ingage Pro ' +
                            (userData.tier === 'web_paid_monthly' ? 'Monthly' : 'Yearly') +
                        ' Subscription</span>' +
                        '<span class="price">$' + invoiceData.initialCost + '</span>' +
                    '</li>' +
                    ( invoiceData.discountValue > 0 ?
                        '<li>' +
                            '<span class="product">Coupon Discount</span>' +
                            '<span class="price">$' + invoiceData.discountValue + '</span>' +
                        '</li>' : ''
                    ) +
                    '<li>' +
                        '<span class="product">Tax</span>' +
                        '<span class="price">$' + invoiceData.taxCost + '</span>' +
                    '</li>' +
                    '<li class="total">' +
                        '<span class="product">TOTAL</span>' +
                        '<span class="price">$' + invoiceData.totalCost + '</span>' +
                    '</li>' +
                '</ul>' +
                '<p class="help">For help, contact <a class="send-email" href="mailto:ingage@scrollmotion.com">ingage@scrollmotion.com</a>.</p>' +
                '<button id="go-to-payment-button" data-ga-label="go-to-payment-button" class="button" value="Apply">Pay Now</button>' +
                ( invoiceData.discountValue > 0 ?
                    '<p class="help">Discounts apply only to the first billing period. Subsequent periods will be billed in full.</p>' : ''
                ) +
                '<a class="monthly-yearly" data-ga-label="change-tier-button" data-tier="web_paid_monthly">Or purchase for $' +
                    invoiceData.altCost + '/' +
                    (userData.tier === 'web_paid_monthly' ? 'yearly' : 'monthly') +
                '</a>'
            );

            this.initElements();
            this.initHandlers();
        },

        calculateInvoiceData: function () {
            var userData      = this.session.getUserData();
            var tier          = userData.tier;
            var couponValue   = userData.couponValue;
            var state         = userData.address.state.toLowerCase();
            var discountValue = 0;
            var taxCost       = 0;
            var tierCost      = TIER_COST[tier === 'free' ? 'web_paid_monthly' : tier];
            var altCost       = ALT_TIER_COST[tier === 'free' ? 'web_paid_monthly' : tier];
            var totalCost     = tierCost;

            var altDiscountValue;

            if (couponValue) {
                if (userData.discountUnit === 'dollar') {
                    discountValue    = couponValue;
                    altDiscountValue = couponValue;
                }

                else {
                    discountValue    = Math.round(tierCost * (couponValue / 100));
                    altDiscountValue = Math.round(altCost * (couponValue / 100));
                }

                totalCost = Math.max((tierCost - discountValue), 0);
                altCost = Math.max((altCost - altDiscountValue), 0);
            }

            // calculate tax for california users
            if (state === 'ca' || state === 'california') {
                taxCost   = totalCost * 0.0775;
                totalCost = totalCost + taxCost;
            }

            // calculate tax for new york users
            else if (state === 'ny' || state === 'new york') {
                taxCost   = totalCost * 0.08875;
                totalCost = totalCost + taxCost;
            }

            return {
                initialCost:   (tierCost / 100).toFixed(2),
                discountValue: (discountValue / 100).toFixed(2),
                taxCost:       (taxCost / 100).toFixed(2),
                totalCost:     (totalCost / 100).toFixed(2),
                altCost:       (altCost / 100).toFixed(2)
            }
        },

        renderPayment: function () {
            this.$content.html(
                '<h1>You are almost done.</h1>' +
                '<p class="description">Upgrade to Ingage Plus. Create unlimited stories of any length. Unlock premium templates.</p>' +
                '<button data-ga-label="close-button" class="close"></button>' +
                '<form id="purchase-form" action="">' +
                    '<input type="tel" pattern="[0-9]*" id="number" placeholder="CC number" />' +
                    '<div>' +
                        '<div class="half">' +
                            '<input type="tel" pattern="[0-9]*" id="cvc" placeholder="CVC" />' +
                        '</div>' +
                        '<div class="half">' +
                            '<input type="tel" pattern="[0-9]*" id="expiry" placeholder="MM / YYYY" />' +
                        '</div>' +
                    '</div>' +
                    '<div class="clear"></div>' +
                    '<div class="error"></div>' +
                    '<input type="submit" data-ga-label="submit-payment-button" data-tier="web_paid_monthly" class="purchase-submit-button" value="Purchase" />' +
                '</form>'
            );

            this.initElements();
            this.initHandlers();
            this.initFormatting();
        },

        initFormatting: function () {
            this.$modal.find('#number').payment('formatCardNumber');
            this.$modal.find('#expiry').payment('formatCardExpiry');
            this.$modal.find('#cvc').payment('formatCardCVC');
        },

        setConfig: function (config) {
            if (config && config.disabled === true) {
                this.setState({ mode: 'purchase' });
                this.disableForm();
                this.error({
                    message: 'You have already purchased Ingage Plus.'
                });
            }

            else {
                this.enableForm();
            }
        },

        handleClick: function (event) {
            event.preventDefault();

            this.clearErrors();
            this.disableForm();

            switch(this.state.mode) {
                case 'coupon':
                    this.validateAddressAndCoupon();
                    break;
                case 'invoice':
                    this.setState({ mode: 'purchase' });
                    break;
                case 'purchase':
                default:
                    var paymentData = this.getPaymentData();

                    this.purchase(paymentData);
                    break;
            }

            return false;
        },

        getAddressData: function () {
            return {
                name:    this.$modal.find('#address-name').val(),
                line1:   this.$modal.find('#address-line1').val(),
                line2:   this.$modal.find('#address-line2').val(),
                city:    this.$modal.find('#address-city').val(),
                state:   this.$modal.find('#address-state').val(),
                zip:     this.$modal.find('#address-zip').val(),
                country: this.$modal.find('#address-country').val()
            }
        },

        getCouponData: function () {
            return {
                coupon: this.$modal.find('#coupon-code').val()
            }
        },

        getPaymentData: function () {
            var purchaseData = {
                number:    this.$modal.find('#number').val(),
                cvc:       this.$modal.find('#cvc').val(),
                exp_month: '',
                exp_year:  ''
            };

            var expirationDate = this.$modal.find('#expiry').val();

            if (expirationDate.match('/')) {
                purchaseData.exp_month = this.$modal.find('#expiry').val().split('/')[0].trim();
                purchaseData.exp_year  = this.$modal.find('#expiry').val().split('/')[1].trim();
            }

            return purchaseData;
        },

        purchase: function (cardData) {
            Promise.all([
                this.validateData(cardData),
                this.validateCard(cardData)
            ]).then(function (values) {
                this.handleValidation.apply(this, values);
            }.bind(this)).catch(this.enableForm.bind(this));
        },

        purchaseWithoutCard: function () {
            var userData = this.session.getUserData();

            $.ajax({
                url: window.scrollmotion.apiUrl + '/smpayments/subscribe/promo/',
                type: 'POST',
                data: {
                    slug:   userData.tier,
                    coupon: userData.couponCode
                },
                crossDomain: true,
                headers: {
                    Authorization: 'Bearer ' + userData.token
                },
                success: this.handleSuccess.bind(this),
                error:   this.handleError.bind(this)
            });
        },

        validateData: function (data) {
            return new Promise(function (resolve, reject) {

                if (this.isDataValid(data)) {
                    resolve(data);
                }

                else {
                    reject('Invalid Data');
                }
            }.bind(this));
        },

        validateAddressAndCoupon: function () {
            var addressData = this.getAddressData();
            var couponData  = this.getCouponData();

            this._validateAddress(addressData)
            .then(function (addressData) {
                this.session.setUserData({
                    address: {
                        name:    addressData.name,
                        line1:   addressData.line1,
                        line2:   addressData.line2,
                        city:    addressData.city,
                        state:   addressData.state,
                        zip:     addressData.zip,
                        country: addressData.country
                    }
                });

                return this._validateCoupon(couponData)
            }.bind(this))
            .then(function (couponData) {
                if (couponData) {
                    this.session.setUserData({
                        couponCode:   couponData.id,
                        couponValue:  couponData.amount_off || couponData.percent_off,
                        discountUnit: couponData.amount_off ? 'dollar' : 'percent'
                    });
                }
                this.setState({ mode: 'invoice' });
            }.bind(this))
            .catch(this.enableForm.bind(this));
        },

        _validateAddress: function (addressData) {
            return new Promise(function (resolve, reject) {
                var hasErrors = false;

                if (addressData.name.trim() === '') {
                    this.error({
                        message: 'Name is required.',
                        id:      '#address-name'
                    });
                    hasErrors = true;
                }

                if (addressData.line1.trim() === '') {
                    this.error({
                        message: 'Address is required',
                        id:      '#address-line1'
                    });
                    hasErrors = true;
                }

                if (addressData.city.trim() === '') {
                    this.error({
                        message: 'City is required.',
                        id:      '#address-city'
                    });
                    hasErrors = true;
                }

                if (addressData.state.trim() === '') {
                    this.error({
                        message: 'State is required.',
                        id:      '#address-state'
                    });
                    hasErrors = true;
                }

                if (addressData.zip.trim() === '') {
                    this.error({
                        message: 'Zip code is required.',
                        id:      '#address-zip'
                    });
                    hasErrors = true;
                }

                if (addressData.country.trim() === '') {
                    this.error({
                        message: 'Country is required.',
                        id:      '#address-country'
                    });
                    hasErrors = true;
                }

                hasErrors ? reject() : resolve(addressData);
            }.bind(this));
        },

        _validateCoupon: function (couponData) {
            var slug     = this.$submit.attr('data-tier');
            var couponId = couponData.coupon.trim();
            var host     = window.scrollmotion.apiUrl;

            return new Promise(function (resolve, reject) {
                if (couponData.coupon.trim() !== '') {
                    $.ajax({
                        url:  host + '/smpayments/coupons/' + couponId,
                        type: 'GET',
                        crossDomain: true,
                        success: function (data, success, response) {
                            if (data.valid) {
                                resolve(data);
                            }

                            else {
                                this.error({
                                    message: 'Coupon \'' + couponId + '\' is invalid.',
                                    id:      '#coupon-code'
                                });

                                reject();
                            }
                        }.bind(this),
                        error:   function (data, status, error) {
                            this.error({
                                message: 'Invalid coupon code',
                                id:      '#coupon-code'
                            });

                            reject(error);
                        }.bind(this)
                    });
                }

                else {
                    resolve();
                }
            }.bind(this));
        },

        validateCard: function (cardData) {
            var userData = this.session.getUserData();
            var address  = userData.address;

            return new Promise(function (resolve, reject) {
                Stripe.card.createToken({
                    number:          cardData.number,
                    cvc:             cardData.cvc,
                    exp_month:       cardData.exp_month,
                    exp_year:        cardData.exp_year,
                    name:            address.name,
                    address_line1:   address.line1,
                    address_line2:   address.line2,
                    address_city:    address.city,
                    address_state:   address.state,
                    address_zip:     address.zip,
                    address_country: address.country
                }, function (status, response) {
                    if (response.id) {
                        var token = response.id;

                        resolve(token);
                    }

                    else {
                        reject(response.error.message);
                    }
                }.bind(this));
            }.bind(this));
        },

        handleValidation: function (data, cardToken) {
            var userData = this.session.getUserData();

            $.ajax({
                url: window.scrollmotion.apiUrl + '/smpayments/subscribe/',
                type: 'POST',
                data: {
                    slug:   userData.tier,
                    token:  cardToken,
                    coupon: userData.couponCode
                },
                crossDomain: true,
                headers: {
                    Authorization: 'Bearer ' + userData.token
                },
                success: this.handleSuccess.bind(this),
                error:   this.handleError.bind(this)
            });
        },

        handleSuccess: function (data, status, response) {
            var userData = this.session.getUserData();

            this.disableForm();

            mixpanel.track('convert-purchase-from-web', {
                eventCategory: 'scrollmotion.com'
            });

            this.session.message('success');
        },

        handleError: function (data, status, error) {
            this.enableForm();

            var response = data.responseJSON;
            var message;

            if (response && response.error && response.error.message) {
                message = response.error.message;
            }

            else if (error) {
                message = error;
            }

            else {
                message = 'An error has occurred';
            }

            this.error({
                message: message
            });
        },

        isDataValid: function (data) {
            var hasErrors = false;

            if (!Stripe.card.validateCardNumber(data.number)) {
                this.error({
                    message: 'Invalid card number.',
                    id:      '#number'
                });
                hasErrors = true;
            }

            if (!Stripe.card.validateExpiry(data.exp_month, data.exp_year)) {
                this.error({
                    message: 'Invalid expiration date. Use format MM/YYYY.',
                    id:      '#expiry'
                });
                hasErrors = true;
            }

            if (!Stripe.card.validateCVC(data.cvc)) {
                this.error({
                    message: 'Invalid CVC number.',
                    id:      '#cvc'
                });
                hasErrors = true;
            }

            return hasErrors === false;
        },

        error: function (error) {
            this.$error.append($('<div>').html(error.message));
            this.$modal.find(error.id).addClass('error');
        },

        clearErrors: function () {
            this.$modal.find('input').each(function (index) {
                if ($(this).attr('type') !== 'submit') {
                    $(this).removeClass('error');
                }
            });

            this.$error.html('');
        },

        clearForm: function () {
            this.clearErrors();

            this.$modal.find('input').each(function (index) {
                if ($(this).attr('type') !== 'submit') {
                    $(this).val('');
                }
            });
        },

        disableForm: function () {
            this.$modal.find('input').each(function (index) {
                $(this).attr('disabled', 'disabled');
            });
        },

        enableForm: function () {
            this.$modal.find('input').each(function (index) {
                $(this).attr('disabled', false);
            });
        }
    };

    window.scrollmotion.Purchase = Purchase;
})();
