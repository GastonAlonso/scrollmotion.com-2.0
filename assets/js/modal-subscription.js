(function () {
    function Subscription (config) {
        this.init(config);

        return this;
    }

    Subscription.prototype = {
        init: function (config) {
            this.$modal  = config.modal;
            this.session = config.session;

            this.$modal.find('.close').on('click', this.hide.bind(this));
            this.$modal.find('.continue-button').on('click', this.hide.bind(this));
        },

        show: function () {
            this.$modal.removeClass('hidden');
        },

        hide: function () {
            this.$modal.addClass('hidden');
        }
    };

    window.scrollmotion.Subscription = Subscription;
})();