<?php
/**
 * The template for displaying a single blog post's content on single-blog.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 */
?>

<div class="blog-content">
    <div class="label-content">
        <label><?php the_time('F j, Y'); ?></label>
    </div>
    <div class="title-content">
        <h1><?php the_title(); ?></h1>
    </div>
    <div class="text-content">
        <?php the_content(); ?>
    </div>
    <div class="back-button link-content" data-ga-action="back to blog">
        <a href="<?php home_url(); ?>/blog" data-ga-label="">
            <div class="back-arrow"></div>Back To The Blog
        </a>
    </div>
</div>