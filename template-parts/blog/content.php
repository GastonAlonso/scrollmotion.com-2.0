<?php
/**
 * Template part for displaying blog post excerpts on index.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */
?>

<div class="blog-content" data-ga-action="post view" data-ga-label="">
    <?php
        $click_handler = "window.location.href='" . get_permalink() . "';";
    ?>
    <?php if ( has_post_thumbnail() ) : ?>
    <div class="post-thumbnail">
        <?php the_post_thumbnail( 'post-thumbnail', array( 'onclick' => $click_handler ) ); ?>
    </div>
    <?php endif; ?>
    <div class="label-content">
        <label onclick="<?php echo $click_handler; ?>">
            <?php the_time('F j, Y') ?>
        </label>
    </div>
    <div class="title-content">
        <h1 onclick="<?php echo $click_handler; ?>">
            <?php the_title() ?>
        </h1>
    </div>
    <div class="text-content" onclick="<?php echo $click_handler; ?>">
        <?php the_excerpt(); ?>
    </div>
    <div class="link-content">
        <a href="<?php the_permalink(); ?>">
            Read More
            <div class="arrow">
                <div></div>
                <div></div>
            </div>
        </a>
    </div>
</div>
