
<?php

// beta/?page=output&mxid=9fd07d78-0039-11e6-89cc-0a2d41c4b523&email=dhyde@scrollmotion.com

$email      = $_GET['email'];
$mxid       = $_GET['mxid'];

include_once('header.php');

?>

<div class="app">
    <div class="app-header">
        <div class="app-header__content">
            <a class="app-header__logo" href="https://www.scrollmotion.com">
                <img class="app-header__icon" src="/wp-content/themes/sm/real-estate-dashboard/icon-ingage.svg?c799ea01b4a84023c010b301925e1775">
                <h1 class="app-header__title">INGAGE BETA</h1>
            </a>
            <slot></slot>
        </div>
    </div>
    <div class="app-content">
        <div class="register">
            <div class="register__content">
                <!-- <form class="register__form" action=""> -->
                <div class="register__form">
                    <h1 class="register__title">We're sorry to see you go</h1>
                    <p style="font-family: Helvetica;margin-top: 0;margin-bottom: 36px;line-height:1.2;">Thank you for having been part of the Ingage Program!</p>
                    <p style="font-family: Helvetica;margin-top: 0;margin-bottom: 36px;line-height:1.2;">Leave by mistake? Click below to rejoin the program.</p>
                    <a href="/beta?page=welcome&mxid=<?php echo urlencode($mxid); ?>&email=<?php echo urlencode($email); ?>" style="text-decoration: none;"><button class="register__submit ingage-button">Re-Join Ingage Beta</button></a>
                </div>
                <!-- </form> -->
            </div>
        </div>
    </div>
</div>

<?php if (isset($mxid)) { ?>
    <script type="text/javascript">
        mixpanel.identify("<?php echo $mxid; ?>");
        mixpanel.people.set({ 'beta-program': false });
        mixpanel.track('opt-out-beta-from-web', {
            eventCategory: 'scrollmotion.com'
        });
    </script>
<?php } ?>

<?php include_once('footer.php'); ?>
