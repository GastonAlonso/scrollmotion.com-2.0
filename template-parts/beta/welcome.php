
<?php

// beta/?page=welcome&mxid=9fd07d78-0039-11e6-89cc-0a2d41c4b523&email=dhyde@scrollmotion.com
// beta/?page=welcome&mxid=e3d05c22-3419-11e7-92a2-0e9f351a8414&email=steven.selig@scrollmotion.com

$email      = $_GET['email'];
$mxid       = $_GET['mxid'];

include('header.php');

?>

<div class="app">
    <div class="app-header">
        <div class="app-header__content">
            <a class="app-header__logo" href="https://www.scrollmotion.com">
                <img class="app-header__icon" src="/wp-content/themes/sm/real-estate-dashboard/icon-ingage.svg?c799ea01b4a84023c010b301925e1775">
                <h1 class="app-header__title">INGAGE BETA</h1>
            </a>
            <slot></slot>
        </div>
    </div>
    <div class="app-content">
        <div class="register">
            <div class="register__content">
                <div class="register__form">
                    <h1 class="register__title">Thank you for signing up!</h1>
                    <p style="font-family: Helvetica;margin-top: 0;margin-bottom: 36px;line-height:1.2;">Thank you for registering for the Ingage Beta Program. We are excited to have you as part of the team helping us to make the best product possible. Please keep an eye out for additional information in the coming days.</p>
                    <p style="font-family: Helvetica;margin-top: 0;margin-bottom: 36px;line-height:1.2;">Join by mistake? Click <a href="/beta?page=optout&mxid=<?php echo urlencode($mxid); ?>&email=<?php echo urlencode($email); ?>" style="color:#2ea1e8;">here</a> to leave the program.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (isset($mxid)) { ?>
    <script type="text/javascript">
        mixpanel.identify("<?php echo urlencode($mxid); ?>");
        mixpanel.people.set({ 'beta-program': true });
        mixpanel.track('join-beta-from-web', {
            eventCategory: 'scrollmotion.com'
        });
    </script>
<?php } ?>

<?php include('footer.php'); ?>
