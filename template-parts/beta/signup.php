
<?php

// beta/?page=signup&fullname=Donald%20Hyde&email=dhyde@scrollmotion.com

$full_name  = $_GET['fullname'];
$email      = $_GET['email'];

include_once('header.php');

?>

<div class="app">
    <div class="app-header">
        <div class="app-header__content">
            <a class="app-header__logo" href="/">
                <img class="app-header__icon" src="/wp-content/themes/sm/real-estate-dashboard/icon-ingage.svg?c799ea01b4a84023c010b301925e1775">
                <h1 class="app-header__title">INGAGE BETA</h1>
            </a>
            <slot></slot>
        </div>
    </div>
    <div class="app-content">
        <div class="register">
            <div class="register__content">
                <form class="register__form" action="">
                    <h1 class="register__title">Welcome to the Ingage Beta Program</h1>
                    <p style="font-family: Helvetica;margin-top: 0;margin-bottom: 36px;line-height:1.2;">Please complete the information below to create your Ingage account and register for the Ingage Beta Testing Program</p>
                    <input class="register__input ingage-input" name="name" placeholder="Name*" type="text" required  value="<?php echo $full_name; ?>" <?php echo isset($full_name) ? '' : 'autofocus="autofocus"' ?> >
                    <input class="register__input ingage-input" name="email" placeholder="Email*" type="email" required value="<?php echo $email; ?>" <?php echo isset($full_name) && isset($email) ? '' : 'autofocus="autofocus"' ?> >
                    <input class="register__input ingage-input" name="password" placeholder="Password*" type="password" required <?php echo !isset($full_name) && !isset($email) ? '' : 'autofocus="autofocus"' ?> >
                    <input class="register__input ingage-input" name="confirm" placeholder="Confirm Password*" type="password" required>
                    <div class="register__password-rules">
                        <label>8 to 30 characters, 1 cap letter, 1 lower letter, 1 digit</label>
                    </div>
                    <div class="register__form-error"><!----></div>
                    <input class="register__submit ingage-button" type="submit" value="Join Ingage Beta">
                </form>
            </div>
        </div>
    </div>
</div>

<script>

    (function () {
        var form            = document.querySelectorAll('form')[0];
        var password        = form.querySelectorAll('input[type=password]')[0];
        var confirmPassword = form.querySelectorAll('input[type=password]')[1];

        password.onchange = validatePassword;
        confirmPassword.onkeyup = validatePassword;

        // Configuration to use the native browser form validation
        function validatePassword(){
            if(password.value !== confirmPassword.value) {
                confirmPassword.setCustomValidity("Passwords Don't Match");
            }

            else {
                confirmPassword.setCustomValidity('');
            }

            var regExp = new RegExp('((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,}))');

            if (!regExp.test(password.value)) {
                password.setCustomValidity('Password must contain 8 to 30 characters, 1 cap letter, 1 lower letter, 1 digit');
            }

            else {
                password.setCustomValidity('');
            }
        }

        // Extract data from the form and format it for sending to the Ingage sign-up API
        function getData (form) {
            var data = {};

            var inputElements = form.querySelectorAll('input');
            var inputElement;
            var inputElementValue;

            for (var i = 0, len = inputElements.length;i < len;i++) {
                inputElement = inputElements[i];
                inputElementValue = inputElement.value;

                switch (inputElement.name) {
                    case 'name':
                        data.first_name = inputElementValue.split(' ')[0];
                        data.last_name  = inputElementValue.split(' ').slice(1).join(' ');
                        break;
                    case 'email':
                        data.username = inputElementValue
                        break;
                    case 'password':
                        data.password = inputElementValue
                        break;
                    default:
                        break;
                }
            }

            return data;
        }

        form.onsubmit = function (e) {
            e.preventDefault();
            form.querySelector('.register__form-error').innerHTML = '';

            // get data for new registration
            var data = getData(e.target);

            $.ajax({
                url:         window.scrollmotion.apiUrl + '/sign_up/',
                type:        'POST',
                data:        data,
                crossDomain: true,
                headers: {
                    'X-CLIENT-ID': window.scrollmotion.clientId
                },
                // create mixpanel user, send mixpanel event, redirect to welcome page
                success: function (data, status, response) {
                    mixpanel.identify(data.id);
                    mixpanel.alias(data.username);

                    mixpanel.people.set({
                        "$firstName":  data.first_name,
                        "$lastName":   data.last_name,
                        "$name":       data.first_name + ' ' + data.last_name,
                        "$created":    new Date(),
                        "$email":      data.username,
                        userId:        data.id,
                        accountId:     data.id,
                        accountName:   data.username
                    });

                    mixpanel.track('convert-reg-beta-from-web', {
                        eventCategory: 'scrollmotion.com'
                    });

                    window.location.href = window.location.origin + window.location.pathname + '?page=welcome&email=' + encodeURIComponent(data.username) + '&mxid=' + encodeURIComponent(data.id);
                },
                error: function (data, status, error) {
                    var errorString = status;

                    if (data.responseJSON) {
                        errorString = getError(data.responseJSON);
                    }

                    form.querySelector('.register__form-error').innerHTML = '<p>' + errorString + '</p>';
                }
            });

            function getError (errorData) {
                var errorString = '';

                if (errorData.errors) {
                    var errors = errorData.errors;

                    for (var i = 0, len = errors.length; i < len; i++) {
                        if (errors[i].code === 'already_exists') {
                            errorString = data.username + ' is already registered with Ingage.<br>'
                        }
                    }
                }

                return errorString;
            }
        }
    })();

</script>

<?php include_once('footer.php'); ?>
