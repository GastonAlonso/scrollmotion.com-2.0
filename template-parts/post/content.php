<?php
/**
 * Template part for displaying post excerpts on index.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */
?>

<div class="press-content" data-ga-action="post view">
    <div class="link-content">
        <a href="<?php the_permalink(); ?>" data-ga-label=""><?php the_title() ?></a>
    </div>
    <div class="label-content">
        <label><?php the_time('F j, Y') ?></label>
    </div>
</div>
