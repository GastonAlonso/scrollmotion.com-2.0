<?php
/**
 * The template for displaying a single post's content on single.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 */
?>

<div class="title-content">
    <h2><?php the_title() ?></h2>
</div>
<div class="label-content">
    <label><?php the_time('F j, Y') ?></label>
</div>
<div class="text-content">
    <?php the_content() ?>
</div>
