<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */

?>

<div class="text-content post-not-found">
    <p>It seems we cant find what youre looking for.</p>
</div>
