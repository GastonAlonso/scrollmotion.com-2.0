<?php
/**
 * Header exclusive to real estate landing page.
 */
?>

<header class="realestate-header" data-ga-category="header">
    <div class="realestate-header__content" data-ga-action="logo">
        <a class="realestate-logo" href="<?php echo home_url(); ?>" data-ga-label="home">
            <img class="realestate-logo__icon" src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-ingage.svg">
            <h1 class="realestate-logo__title realestate-logo__title--small">Ingage</h1>
            <h1 class="realestate-logo__title realestate-logo__title--large">Ingage Real Estate</h1>
        </a>
        <ul class="realestate-nav" data-ga-action="nav">
            <li id="see-examples" class="realestate-nav__item">
                <a class="realestate-nav__link page-scroller" data-anchor-id="realestate-examples-anchor" href="#" data-ga-label="examples">see examples</a>
            </li>
            <li class="realestate-nav__item realestate-nav__item--boxed realestate-getstarted">
                <a class="realestate-nav__link" href="javascript:void(0)" data-ga-label="get-started">Get Started</a>
            </li>
            <li class="realestate-nav__item realestate-nav__item">
                <a class="realestate-nav__link" href="<?php echo home_url(); ?>/real-estate-dashboard/" data-ga-label="login">Log in</a>
            </li>
        </ul>
    </div>
</header>
