<?php
/**
 * Displays header logo
 */
?>
<a class='header__logo' href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo( 'name' ); ?>" data-ga-label="home">
    <img class='header-logo__icon-classic' src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-scrollmotion.png" alt="Scrollmotion Logo" />
</a>
