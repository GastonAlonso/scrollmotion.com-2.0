<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */
?>

<section class="article-page">
    <div class="section-content">
        <header class="title-content">
            <h2><?php the_title(); ?></h2>
            <?php if ( is_page( 'privacy' ) ) : ?>
                <h3>Effective <?php the_time('F j, Y'); ?></h3>
            <?php elseif ( is_page( 'terms' ) ) : ?>
                <h3>Revised: <?php the_time('F j, Y'); ?></h3>
            <?php endif; ?>
        </header>
        <div class="text-content">
            <?php the_content(); ?>
        </div>
    </div>
</section>
