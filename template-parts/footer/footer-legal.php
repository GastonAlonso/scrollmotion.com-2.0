<?php
/**
 * Displays footer legal message
 */
?>

<p class='footer__legal'>&copy; 2010&ndash;17 ScrollMotion, Inc. All Rights Reserved.</p>

<?php if ( is_page( 'realestate' ) ) : ?>
    <p class='footer__legal'>Contact <a data-ga-label="email" href="mailto:support@scrollmotion.com">support@scrollmotion.com</a> for help.</p>
<?php endif;
