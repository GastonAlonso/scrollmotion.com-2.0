<?php
/**
 * Displays footer social icons
 */
?>

<ul class="footer__social">
    <li class='footer__social-item'>
        <a href="https://www.facebook.com/Scrollmotion/" title="Share on Facebook" data-ga-label="facebook">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/facebook.svg" alt="Facebook Icon" />
        </a>
    </li>
    <li class='footer__social-item'>
        <a href="https://twitter.com/scrollmotion" title="Share on Twitter" data-ga-label="twitter">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/twitter.svg" alt="Twitter Icon" />
        </a>
    </li>
    <li class='footer__social-item'>
        <a href="https://www.pinterest.com/scrollmotion/" title="Share on Pinterest" data-ga-label="pinterest">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/pinterest.svg" alt="Pinterest Icon" />
        </a>
    </li>
    <li class='footer__social-item'>
        <a href="https://www.instagram.com/scrollmotion/" title="Share on Instagram" data-ga-label="instagram">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/instagram.svg" alt="Instragram Icon" />
        </a>
    </li>
    <li class='footer__social-item'>
        <a href="https://www.linkedin.com/company-beta/348827" title="Share on LinkedIn" data-ga-label="linkedin">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/linkedin.svg" alt="LinkedIn Icon" />
        </a>
    </li>
    <li class='footer__social-item'>
        <a href="https://www.youtube.com/user/ScrollMotionTM" title="Share on YouTube" data-ga-label="youtube">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/youtube.svg" alt="Youtube Icon" />
        </a>
    </li>
</ul>
