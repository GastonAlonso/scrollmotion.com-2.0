<?php
/**
 * Displays footer logo and apple store logo
 */

if ( is_page( 'realestate' ) ) : ?>

    <div class="footer__logos footer__logos--realestate">
        <a class="footer__ingage-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" data-ga-label="realtors">
            <img class="footer__ingage-icon" src="<?php echo get_template_directory_uri() . '/assets/images/icon-ingage.svg'; ?>" alt="ingage-icon">
            <h1 class="footer__branding">Ingage Real Estate</h1>
        </a>
        <a class="footer__button realestate-getstarted" href="<?php echo home_url(); ?>/real-estate-dashboard/#/register" data-ga-label="register">
            Get Started
        </a>
    </div>

<?php else : ?>

    <div class="footer__logos" data-ga-action="logos">
        <a class="footer__ingage-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" data-ga-label="home">
            <img class="footer__ingage-icon" src="<?php echo get_template_directory_uri() . '/assets/images/icon-ingage.svg'; ?>" alt="ingage-icon">
            <h1 class="footer__branding">Ingage for iPad</h1>
        </a>
        <a href="https://itunes.apple.com/app/ingage-interactive-presentations/id1100355870?mt=8#" data-ga-label="app-store">
            <img class="footer__app-store" src="<?php echo get_template_directory_uri() . '/assets/images/download-on-the-app-store.svg'; ?>" alt="apple store" />
        </a>
    </div>

<?php endif;
