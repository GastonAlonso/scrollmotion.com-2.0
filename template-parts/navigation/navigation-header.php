<?php
/**
 * Displays header navigation
 *
 * @link https://developer.wordpress.org/reference/functions/wp_nav_menu/
 */
?>
<nav class='header__nav'>
    <?php
        wp_nav_menu( array(
            'theme_location' => 'header-nav',
            'container'      => '',                // remove 'div' container.
            'menu_class'     => 'header-nav__menu' // overwrite default class here.
        ) );
    ?>
</nav>