<?php
/**
 * Displays footer navigation
 *
 * @link https://developer.wordpress.org/reference/functions/wp_nav_menu/
 */
?>
<nav class='footer__nav'>
    <?php
        wp_nav_menu( array(
            'theme_location' => 'footer-nav',
            'container'      => '', // remove 'div' container.
            'menu_class'     => 'footer-nav__menu' // overwrite default class here.
        ) );
    ?>
</nav>
