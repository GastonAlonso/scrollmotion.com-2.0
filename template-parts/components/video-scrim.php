<div class="video__scrim">
    <div class="scrim__close">
        <div></div>
        <div></div>
    </div>
    <video class="scrim__video video-focus" playsinline controls></video>
</div>
