<?php
/*
 * Registration modal specific to Real Estate page.
 */
?>

<div id="realestate-modal" class="realestate-modal" style="display:none">
    <div class="realestate-modal__container">
        <button class="realestate-modal__close close-button"></button>
        <h1 class="realestate-modal__title">One amazing listing.<br>Three simple steps. $99.</h1>
        <ul class="realestate-modal__step-list">
            <li class="realestate-modal__step">
                <div class="realestate-modal__step-number">1</div><p>Register for an Ingage account and purchase your Ingage real estate listing.</p>
            </li>
            <li class="realestate-modal__step">
                <div class="realestate-modal__step-number">2</div><p>Upload pictures, videos, floor plans, and descriptions about your property through our guided upload modal.</p>
            </li>
            <li class="realestate-modal__step">
                <div class="realestate-modal__step-number">3</div><p>We'll email you when your listing is ready, usually within 48 hours. We work with you until it's perfect.</p>
            </li>
        </ul>
        <a class="realestate-modal__link" href="/real-estate-dashboard/#/register?redirect=address">Yes, I'd like to purchase</a>
    </div>
</div>
