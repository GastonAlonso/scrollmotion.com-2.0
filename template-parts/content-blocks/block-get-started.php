<?php
/**
 * Template for the Real Estate "Get Started" block
 * It's a clone of the "Title Text Button" block for Real Estate purposes
 */
?>

<?php
    $theme_class = '';

    if ( get_sub_field( 'theme' ) === 'light'  ) :
        $theme_class = 'light';
    else :
        $theme_class = 'dark';
    endif;
?>

<?php
    $analytics_action = get_sub_field( 'analytics_action' );

    if (!$analytics_action) {
        $analytics_action = 'get-started';
    }
?>

<section class="title-text-button title-text-button--<?php echo $theme_class; ?>" data-ga-action="<?php echo $analytics_action; ?>">
    <?php if ( get_sub_field( 'title' ) ) : ?>
        <h2 class="title-text__title title-text-button__title title-text-button__title--<?php echo $theme_class; ?>"><?php the_sub_field( 'title' ); ?></h2>
    <?php endif; ?>
    <?php if ( get_sub_field( 'text' ) ) : ?>
        <p class="title-text__text title-text-button__text title-text-button__text--<?php echo $theme_class; ?>">
            <?php the_sub_field( 'text' ); ?>
        </p>
    <?php endif; ?>
    <?php if ( have_rows( 'button') ) : ?>
        <?php while ( have_rows( 'button' ) ) : the_row();
            $link_url = '';

            if ( get_sub_field( 'link_type' ) == 'url') :
                $link_url = get_sub_field( 'url_link' );
            else :
                $link_url = get_sub_field( 'page_link' );
            endif;
        ?>
            <a class="title-text-button__button realestate-getstarted" href="<?php echo $link_url; ?>" target="_blank" data-ga-label="button"><?php the_sub_field( 'button_text' ); ?></a>
        <?php endwhile; ?>
    <?php endif; ?>
</section>
