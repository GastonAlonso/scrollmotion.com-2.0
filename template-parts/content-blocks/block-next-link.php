<?php
/**
 * Template for the Next Link content block.
 */
?>
<?php
    $nextLink_class = 'next-link';

    if ( !get_sub_field( 'image' ) ) :
        $nextLink_class .= ' next-link--blue';
    endif;
?>

<?php
    $analytics_action = get_sub_field( 'analytics_action' );

    if (!$analytics_action) {
        $analytics_action = 'next-link';
    }
?>

<section class="<?php echo $nextLink_class; ?>" data-ga-action="<?php echo $analytics_action; ?>">
    <?php
        $button_link = '';

        if ( get_sub_field( 'link_type' ) == 'url') :
            $button_link = get_sub_field( 'url_link' );
        else :
            $button_link = get_sub_field( 'page_link' );
        endif;
    ?>
    <a class="next-link__link" href="<?php echo $button_link ?>" data-ga-label="link">
        <div class="link__image" style="background-image: url(<?php
            checkIfFieldValueIsIDAndReturnTheURLAndDeleteThisFunctionLaterLikeReallyDoIt(get_sub_field( 'image' ), 'medium');
        ?>)"></div>
        <h2 class="link__title"><?php the_sub_field( 'title' ) ?></h2>
        <span class="link__subtitle"><?php the_sub_field( 'subtitle' ) ?></span>
    </a>
</section>
