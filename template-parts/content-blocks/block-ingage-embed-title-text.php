<?php
/**
 * Template for the Ingage Embeded Widget Link content block.
 */
?>

<section class="ingage-embed-title-text">    
    <?php get_template_part('/template-parts/content-blocks/block-ingage-embed/ingage-embed') ?>
    <h2 class="title-text__title embed-title-text__title"><?php the_sub_field( 'title' ) ?></h2>
    <p class="title-text__text embed-title-text__text"><?php the_sub_field( 'text') ?></p>
</section>