<?php
/**
 * Template for the Tutorials content block.
 */
?>

<?php
    $analytics_action = get_sub_field( 'analytics_action' );

    if (!$analytics_action) {
        $analytics_action = 'tutorials';
    }
?>

<section class="tutorials" data-ga-action="<?php echo $analytics_action; ?>">
    <h2 class="title-text__title tutorials__title"><?php the_sub_field( 'title' ); ?></h2>
    <p class="title-text__text tutorials__title"><?php the_sub_field( 'text' ) ?></p>
    <?php if ( have_rows( 'videos') ) : ?>
        <div class="tutorials__videos">
            <?php while ( have_rows( 'videos' ) ) : the_row();?>
                <div class="videos__video-block">
                    <div class='video-block__video-container'>
                        <div class="video-container__thumbnail"
                            style="background-image: url(<?php
                                checkIfFieldValueIsIDAndReturnTheURLAndDeleteThisFunctionLaterLikeReallyDoIt(get_sub_field('thumbnail'), 'thumbnail');
                            ?>);"
                        ></div>
                        <div data-video='<?php the_sub_field('video')?>' class="video-container__play-button-wrapper video-play-button">
                            <img class='video-container__play-button' src="<?php echo get_template_directory_uri() . '/assets/images/play-button-v2.svg'; ?>" alt="play-button" data-ga-label="<?php echo strtolower(get_sub_field( 'video_title' )); ?>button" />
                        </div>
                    </div>
                    <div class='video-block__content'>
                        <p class="video-block__title"><?php the_sub_field( 'video_title' ); ?></p>
                        <p class="video-block__duration"><?php the_sub_field( 'video_duration' ); ?></p>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    <?php endif; ?>
</section>
