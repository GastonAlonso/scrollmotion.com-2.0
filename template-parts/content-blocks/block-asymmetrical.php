<?php
/**
 * Template for the Asymmetrical content block.
 */
?>

<?php
    $direction_class = '';

    if ( get_sub_field( 'direction' ) === 'left'  ) :
        $direction_class = 'left';
    else :
        $direction_class = 'right';
    endif;
?>

<?php
    $analytics_action = get_sub_field( 'analytics_action' );

    if (!$analytics_action) {
        $analytics_action = 'asymmetrical';
    }
?>

<section class="asymmetrical asymmetrical--<?php echo $direction_class; ?>" style="background: <?php the_sub_field( 'background_color' ) ?>" data-ga-action="<?php echo $analytics_action; ?>">
    <div class="asymmetrical__image" style='background-image: url(<?php
        checkIfFieldValueIsIDAndReturnTheURLAndDeleteThisFunctionLaterLikeReallyDoIt(get_sub_field( 'image' ), 'large');
    ?>)'>
    </div>
    <div class="asymmetrical__text-wrapper">
        <h2 class="asymmetrical__header" style="color: <?php the_sub_field( 'text_color' ) ?>"><?php the_sub_field( 'header' ) ?></h2>
        <div class="asymmetrical__content" style="color: <?php the_sub_field( 'text_color' ) ?>"><?php the_sub_field( 'content' ) ?></div>
        <?php if ( get_sub_field( 'button' ) ) : ?>
            <?php
                $button_link = '';

                if ( get_sub_field( 'link_type' ) == 'url') :
                    $button_link = get_sub_field( 'url_link' );
                else :
                    $button_link = get_sub_field( 'page_link' );
                endif;
            ?>
            <a class="asymmetrical__link" href="<?php echo $button_link ?>"" target="_blank" data-ga-label="button"><?php the_sub_field('button_text') ?></a>
        <?php endif; ?>
    </div>
</section>
