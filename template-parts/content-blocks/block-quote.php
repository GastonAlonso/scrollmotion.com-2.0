<?php
/**
 * Template for the Quote content block.
 */
?>

<?php
    $theme_class = '';

    if ( get_sub_field( 'theme' ) === 'light' ) :
        $theme_class = 'light';
    else :
        $theme_class = 'dark';
    endif;
?>

<section class="quote quote--<?php echo $theme_class; ?>">
    <blockquote class="quote__text">
        <?php the_sub_field( 'quote' ) ?>
    </blockquote>
    <p class="quote__author">
        <?php the_sub_field( 'author' ) ?>
    </p>
</section>
