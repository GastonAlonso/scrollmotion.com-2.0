<?php
/**
 * Template for the Video Title Text content block.
 */
?>

<?php
    $theme_class = get_sub_field( 'theme' );
?>

<?php
    $padding_class = '';

    if ( get_sub_field( 'padding' ) ) :
        $padding_class .= 'video-title-text--has-padding';
    endif;
?>

<section class='video-title-text video-title-text--<?php echo $theme_class; ?> <?php echo $padding_class; ?>'>
    <div class="video-title-text__media">
        <?php if ( get_sub_field( 'video' ) ) : ?>
            <video class='video-title-text__video video-focus' src="<?php the_sub_field( 'video' ); ?>" autoplay playsinline loop muted></video>
        <?php endif; ?>
        <?php if ( get_sub_field( 'image' ) ) : ?>
            <img class='video-title-text__image' src="<?php the_sub_field( 'image' ); ?>">
        <?php endif; ?>
    </div>
    
    <h2 class='title-text__title video-title-text__title video-title-text__title--<?php echo $theme_class; ?>'><?php the_sub_field( 'title' ); ?></h2>
    <p class='title-text__text video-title-text__text video-title-text__text--<?php echo $theme_class; ?>'><?php the_sub_field( 'text' ) ?></p>
</section>
