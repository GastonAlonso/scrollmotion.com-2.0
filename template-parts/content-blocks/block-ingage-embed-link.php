<?php
/**
 * Template for the Ingage Embeded Widget Link content block.
 */
?>

<?php
    $analytics_action = get_sub_field( 'analytics_action' );

    if (!$analytics_action) {
        $analytics_action = 'ingage-embed-link';
    }
?>

<section class="ingage-embed-link" data-ga-action="<?php echo $analytics_action; ?>">
    <?php get_template_part('/template-parts/content-blocks/block-ingage-embed/ingage-embed') ?>
    <div class="ingage-embed-link__content">
        <h2 class="embed-link__title"><?php the_sub_field( 'title' ) ?></h2>
        <a href="<?php the_sub_field( 'url') ?>" target="_blank" class="embed-link__link" data-ga-label="link"><?php the_sub_field( 'button_text' ) ?></a>
    </div>
</section>