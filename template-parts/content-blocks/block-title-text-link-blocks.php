<?php
/**
 * Template for the Title Text Link Blocks content block.
 */
?>

<?php
    $theme_class = '';

    if ( get_sub_field( 'theme' ) === 'dark'  ) :
        $theme_class = 'link-blocks--dark';
    endif;
?>

<?php
    $analytics_action = get_sub_field( 'analytics_action' );

    if (!$analytics_action) {
        $analytics_action = 'title-text-link-blocks';
    }
?>

<section class="link-blocks <?php echo $theme_class; ?>" data-ga-action="<?php echo $analytics_action; ?>">
    <h2 class="title-text__title link-blocks__title"><?php the_sub_field( 'title' ); ?></h2>
    <p class="title-text__text link-blocks__subtitle"><?php the_sub_field( 'text' ) ?></p>
    <?php if ( have_rows( 'link_blocks') ) : ?>
        <div class="link-blocks__blocks">
            <?php while ( have_rows( 'link_blocks' ) ) : the_row();
                $link_url = '';

                if ( get_sub_field( 'link_type' ) == 'url') :
                    $link_url = get_sub_field( 'url_link' );
                else :
                    $link_url = get_sub_field( 'page_link' );
                endif;
            ?>
                <a class="blocks__block" href="<?php echo $link_url; ?>" data-ga-label="<?php echo strtolower(get_sub_field( 'link_text' )); ?>-link">
                  <div class="block__image" style="background-image: url('<?php
                    checkIfFieldValueIsIDAndReturnTheURLAndDeleteThisFunctionLaterLikeReallyDoIt(get_sub_field( 'link_background' ), 'medium');
                  ?>');"></div>
                  <h3 class="block__title"><?php the_sub_field( 'link_text' ); ?></h3>
                </a>
            <?php endwhile; ?>
        </ul>
    <?php endif; ?>
</section>
