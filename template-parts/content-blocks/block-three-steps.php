<?php
/**
 * Template for the "Three Steps" content block
 */
?>

<section class="three-steps">
    <div class="three-steps__content">
        <div class="three-steps__title">
            <h1><?php the_sub_field( 'title' ); ?></h1>
        </div>
        <?php if ( have_rows( 'steps' ) ) : ?>
            <div class="three-steps__steps">
                <?php
                    $count = 0;

                    while ( have_rows( 'steps' ) ) : the_row();
                ?>
                    <div class="three-steps__step">
                        <div class="three-steps__number">
                            <span><?php echo ++$count ?></span>
                        </div>
                        <div class="three-steps__text">
                            <?php the_sub_field( 'step_text' ) ?>
                        </div>
                    </div>
                <?php endwhile; ?>
           </div>
        <?php endif; ?>
   </div>
</section>
