<?php
/**
 * Template for the Hero content block.
 */
?>
<?php
    $hero_class = 'hero';

    if ( get_sub_field( 'play' ) ) :
        $hero_class .= ' hero--play';
    endif;

    if ( get_sub_field( 'appstore_button' ) ) :
        $hero_class .= ' hero--button';
    endif;
?>

<?php
    $analytics_action = get_sub_field( 'analytics_action' );

    if (!$analytics_action) {
        $analytics_action = 'hero';
    }
?>

<section class='<?php echo $hero_class; ?>' data-ga-action="<?php echo $analytics_action; ?>">
    <?php if ( get_sub_field( 'background_image' ) ) : ?>
        <div class='hero__image' style="background-image: url(<?php
            checkIfFieldValueIsIDAndReturnTheURLAndDeleteThisFunctionLaterLikeReallyDoIt(get_sub_field( 'background_image' ), 'large');
        ?>)"></div>
    <?php endif; ?>

    <?php if ( get_sub_field( 'background_video' ) ) : ?>
        <div class="hero__video-wrapper">
            <video class='hero__video mutable-video video-focus' src="<?php the_sub_field( 'background_video' ); ?>" muted playsinline autoplay loop></video>
        </div>
    <?php endif; ?>

    <div class='hero__content'>
        <h2 class='hero__title'><?php the_sub_field( 'header' ) ?></h2>
        <div class='hero__subtitle'>
            <?php the_sub_field( 'content' ) ?>
        </div>

        <?php if ( get_sub_field( 'play' ) ) : ?>
            <div data-video="<?php the_sub_field( 'background_video' ); ?>" class="hero__play-button video-play-button" data-ga-label="play-button">
                <img class="play-button__play" src="<?php echo get_template_directory_uri() . '/assets/images/icon_play_lg@2x.png'; ?>"/>
                <span class="play-button__duration"><?php echo the_sub_field('video_time'); ?></span>
            </div>
        <?php endif; ?>

        <?php if ( get_sub_field( 'appstore_button' ) ) : ?>
            <a href="https://itunes.apple.com/app/ingage-interactive-presentations/id1100355870?mt=8#" data-ga-label="app-store-button">
                <img class='hero__app-store' src="<?php echo get_template_directory_uri() . '/assets/images/download-on-the-app-store.svg'; ?>" alt="apple store" />
            </a>
        <?php endif; ?>

        <?php if ( get_sub_field( 'link' ) ) :
            $link_url = '';

            if ( get_sub_field( 'link_type' ) == 'url') :
                $link_url = get_sub_field( 'link_url' );
            else :
                $link_url = get_sub_field( 'link_page' );
            endif;
        ?>
            <a class="hero__link realestate-getstarted" href="<?php echo $link_url; ?>" data-ga-label="link"><?php the_sub_field( 'link_text' ); ?></a>
        <?php endif; ?>
    </div>

    <?php if ( get_sub_field( 'sound_toggle' ) ) : ?>
        <button class='mutable-toggle muted hero__sound-toggle' data-ga-label="sound-toggle-button"></button>
    <?php endif; ?>

    <?php if ( get_sub_field( 'chevron' ) ) : ?>
        <a class="hero__chevron-wrapper page-scroller" data-anchor-id="<?php the_sub_field( 'anchor_id' ); ?>" href="#" data-ga-label="chevron">
            <div class="hero__chevron-down"></div>
        </a>
    <?php endif; ?>
</section>
