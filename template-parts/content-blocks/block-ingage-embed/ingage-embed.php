<?php
/**
 * Template for the Ingage Embeded Widget content block.
 */
    wp_enqueue_script('ingage');
?>
 
 <div class='ingage-embed'>
    <?php /* includes the correct json stub for the widget */ ?>
    <?php if (have_rows('widget_type')): ?>
        <?php while (have_rows('widget_type')): the_row(); ?>
            <script class="ingage-embed__stub" type="text/x-ingage-config">
                <?php get_template_part('template-parts/content-blocks/block-ingage-embed/block', get_row_layout()); ?>
            </script>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php /* widget container */ ?>
    <div class="ingage-embed__container">
        <div class="ingage-embed__view"></div>
    </div>
</div>

    <!-- <iframe class="ingage-embed__view" src="<?php echo get_template_directory_uri(); ?>/template-parts/content-blocks/block-ingage-embed/frame.html"></iframe> -->

<script>
    $(function () {
        $('.ingage-embed').each(function (i, e) {
            var $stub   = $(e).find('.ingage-embed__stub');
            var $view   = $(e).find('.ingage-embed__view');
            // var elEmbed = $view.contents().find('#embed').get(0);
            var elEmbed = $(e).find('.ingage-embed__view').get(0);
            var stub    = JSON.parse($stub.text());

            new Ingage.WebEmbed({
                target: elEmbed,
                stub:   stub
            });
        });
    });
</script>
