{
    "template": "embed",
    "version":  "1.0",
    "properties": {
        "widget": {
            "template": "compare",
            "version": "1.0",
            "properties": {
                "compare": [
                    "<?php the_sub_field('image_a'); ?>",
                    "<?php the_sub_field('image_b'); ?>"
                ],
                "slideDirection": "<?php echo strtolower(get_sub_field('orientation')); ?>",
                "slidePosition":  <?php the_sub_field('position'); ?>
            }
        }
    }
}