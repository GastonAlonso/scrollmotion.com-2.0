{
    "widget": "pointsOfInterest",
    "version": "1.0",
    "properties": {
        "background": "templates/pointsOfInterest/1.0/assets/background.jpg",
        "points": [
            {
                "type": "blurb",
                "properties": {
                    "text": "This balloon is made of coballoon is made of coballoon is made of coballoon is made of coballoon is made of colorful material",
                    "x": "5.6%",
                    "y": "90.5%"
                }
            },
            {
                "type": "blurb",
                "properties": {
                    "text": "Text Point",
                    "x": "99.6%",
                    "y": "31.7%"
                }
            },
            {
                "type": "blurb",
                "properties": {
                    "text": "Text Point",
                    "x": "80.6%",
                    "y": "31.7%"
                }
            },
            {
                "type": "video",
                "properties": {
                    "video": "templates/pointsOfInterest/1.0/assets/video.mov",
                    "title": "Video Point",
                    "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                    "x": "49.7%",
                    "y": "80.8%"
                }
            },
            {
                "type": "video",
                "properties": {
                    "video": "templates/pointsOfInterest/1.0/assets/portrait.mp4",
                    "title": "Video Point",
                    "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                    "x": "29.7%",
                    "y": "80.8%"
                }
            },
            {
                "type": "image",
                "properties": {
                    "image": "templates/pointsOfInterest/1.0/assets/image.jpg",
                    "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                    "x": "52.1%",
                    "y": "61.1%"
                }
            }
        ]
    }
}