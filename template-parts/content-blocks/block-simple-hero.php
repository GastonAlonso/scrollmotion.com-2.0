<?php
/**
 * Template for the Simple Hero content block.
 */
?>

<section class="basic-hero">
    <div class="section-content">
        <h1><?php the_sub_field( 'title' ) ?></h1>
        <h3><?php the_sub_field( 'subtitle' ) ?></h3>
    </div>
</section>