<?php
/**
 * Template for the Title Text Link Blocks content block.
 * ALL FIELDS OPTIONALS FOR THIS TEMPLATE
 */
?>

<?php
    $theme_class = '';

    if ( get_sub_field( 'theme' ) === 'light'  ) :
        $theme_class = 'light';
    else :
        $theme_class = 'dark';
    endif;
?>

<?php
    $analytics_action = get_sub_field( 'analytics_action' );

    if (!$analytics_action) {
        $analytics_action = 'title-text-button';
    }
?>

<section class="title-text-button title-text-button--<?php echo $theme_class; ?>" data-ga-action="<?php echo $analytics_action; ?>">
    <?php if ( get_sub_field( 'title' ) ) : ?>
        <h2 class="title-text__title title-text-button__title title-text-button__title--<?php echo $theme_class; ?>"><?php the_sub_field( 'title' ); ?></h2>
    <?php endif; ?>
    <?php if ( get_sub_field( 'text' ) ) : ?>
        <p class="title-text__text title-text-button__text title-text-button__text--<?php echo $theme_class; ?>">
            <?php the_sub_field( 'text' ); ?>
        </p>
    <?php endif; ?>
    <?php if ( have_rows( 'button') ) : ?>
        <?php while ( have_rows( 'button' ) ) : the_row();
            $link_url = '';

            if ( get_sub_field( 'link_type' ) == 'url') :
                $link_url = get_sub_field( 'url_link' );
            else :
                $link_url = get_sub_field( 'page_link' );
            endif;
        ?>
            <?php
                $target = "";
                if ( get_sub_field( 'new_tab' ) ) {
                    $target = "_blank";
                }
            ?>
            <a class="title-text-button__button" href="<?php echo $link_url; ?>" target="<?php echo $target; ?>" data-ga-label="button"><?php the_sub_field( 'button_text' ); ?></a>
        <?php endwhile; ?>
    <?php endif; ?>
</section>