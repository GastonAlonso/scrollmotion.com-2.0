<?php
/**
 * Template for the Editorial Text content block.
 */
?>

<section class='editorial'>
    <div class='editorial__content'> 
        <h2 class='editorial__title'><?php the_sub_field( 'title' ); ?></h2>
        <p class='editorial__body'><?php the_sub_field( 'text' ); ?><p>
    </div>
</section>
