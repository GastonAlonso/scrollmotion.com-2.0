<?php
/**
 * Template for the Top Video Title Text content block.
 */
?>

<?php
    $topVideo_class = 'top-video-title-text';
?>

<?php
    $theme_class   = '';
    $padding_class = '';

    if ( get_sub_field( 'theme' ) === 'light' ) :
        $theme_class = 'light';
    else :
        $theme_class = 'dark';
    endif;

    if ( get_sub_field('padding_top') ) {
        $padding_class .= ' top-video-title-text--padding-top';
    }

    if ( get_sub_field('padding_bottom') ) {
        $padding_class .= ' top-video-title-text--padding-bottom';
    }
?>

<?php
    $analytics_action = get_sub_field( 'analytics_action' );

    if (!$analytics_action) {
        $analytics_action = 'top-video-title-text';
    }
?>

<section class='<?php echo $topVideo_class . ' ' . $topVideo_class . '--' . $theme_class . ' ' . $padding_class; ?>' data-ga-action="<?php echo $analytics_action; ?>">
    <div class='top-video-title-text__video-container top-video-container'>
        <?php if ( get_sub_field( 'user_interaction' ) === 'sound' ) : ?>
            <video class='mutable-video top-video-title-text__video video-focus' src="<?php the_sub_field( 'video' ); ?>" autoplay playsinline loop muted></video>
            <button class='mutable-toggle muted video-container__sound' />
        <?php elseif ( get_sub_field ('user_interaction' ) === 'play' ) : ?>
            <video class='mutable-video top-video-title-text__video top-video' style="display: none;" src="<?php the_sub_field( 'video' ); ?>" playsinline controls></video>
            <img class="top-video-title-text__video-still top-video-still" src="<?php the_sub_field( 'video_still' ); ?>" />
            <div class="top-video-title-text__play-button-wrapper top-video-play-button">
                <img class="top-video-title-text__play-button" src="<?php echo get_template_directory_uri() . '/assets/images/play-button-v2.svg'; ?>" data-ga-label="play" alt="play-button"/>
            </div>
        <?php else : ?>
            <video class='mutable-video top-video-title-text__video video-focus' src="<?php the_sub_field( 'video' ); ?>" autoplay playsinline loop muted></video>
        <?php endif; ?>
    </div>
    <?php if ( trim(get_sub_field( 'title' )) != '' ) : ?>
        <h2 class='title-text__title top-video-title-text__title'><?php the_sub_field( 'title' ); ?></h2>
    <?php endif; ?>
    <?php if ( trim(get_sub_field( 'text' )) != '' ) : ?>
        <p class='title-text__text top-video-title-text__text '><?php the_sub_field( 'text' ) ?></p>
    <?php endif; ?>
    <?php if ( get_sub_field( 'call_to_action_button' ) ) : ?>
        <a
            href='<?php the_sub_field( 'call_to_action_button_link' ); ?>'
            class='top-video-title-text__button title-text-button__button<?php echo get_sub_field( 'realestate_modal' ) ? ' realestate-getstarted' : '' ?>'
            <?php if ( get_sub_field( 'call_to_action_button_open_in_new_tab' ) ) : ?>
                <?php echo "target='_blank'"; ?>
            <?php endif; ?>
        >
            <?php the_sub_field( 'call_to_action_button_text' ) ?>
        </a>
    <?php endif; ?>
</section>
