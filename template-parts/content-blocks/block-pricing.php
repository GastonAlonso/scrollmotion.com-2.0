<?php
/**
 * Template for the Pricing content block.
 */
?>

<?php
    $padding_class = '';

    if ( get_sub_field('padding_top') ) {
        $padding_class = 'tiers--padding-top';
    }
?>

<section class="tiers <?php echo $padding_class; ?>" data-ga-action="tiers">
    <div class="section-content">
        <div class="free-full">
            <div class="spot free"></div>
            <h1>Ingage Free</h1>
            <ul>
                <li>3 stories</li>
                <hr />
                <li>10 pages/ story</li>
                <hr />
                <li>Share stories up to 200 MB</li>
                <hr />
                <li>2 GB storage</li>
                <hr />
                <li>Basic page templates</li>
            </ul>
            <button data-tier="free" data-ga-label="free" class="button new-registration">Get free account</button>
        </div>
        <div class="plus-full">
            <div class="spot plus"></div>
            <h1>Ingage Plus</h1>
            <ul class="checkmark">
                <li>Unlimited stories</li>
                <hr />
                <li>Unlimited pages</li>
                <hr />
                <li>Share stories up to 500 MB</li>
                <hr />
                <li>5 GB storage</li>
                <hr />
                <li>Premium page templates<span>Mailing list integration</span></li>
            </ul>
            <button data-tier="web_paid_monthly" data-ga-label="monthly" class="button new-registration">$14.99/Month</button>
            <button data-tier="web_paid_yearly" data-ga-label="yearly" class="button new-registration yearly">$129.99/Year</button>
            <a class="about-subscriptions" data-ga-label="about subscriptions">About subscriptions</a>
        </div>
        <div class="free-small">
            <p>
                Not ready for Plus?<br>
                You can upgrade later.
            </p>
            <button data-tier="free" data-ga-label="free" class="button new-registration">Start with free account</button>
        </div>
    </div>
</section>

<div id="register-modal" class="modal hidden" data-ga-action="register">
    <div class="modal-content">
        <h1>Go Global.</h1>
        <p class="description">Register with Ingage to save your story to the cloud and share with users worldwide.</p>
        <button class="close" data-ga-label="close"></button>
        <a class="sign-in sign-in-button" data-ga-label="login">Already have an account? <span>Sign in to upgrade to Ingage Plus.</span></a>
        <form id="register-form" method="post" action="">
            <input type="text" id="first-name" name="first_name" placeholder="First name" />
            <input type="text" id="last-name" name="last_name" placeholder="Last name" />
            <input type="email" id="email" name="username" placeholder="Email" />
            <input type="password" id="password" name="password" placeholder="Password" />
            <p>1 cap letter &nbsp;1 lower letter &nbsp;1 digit &nbsp;8 to 30 characters</p>
            <input type="password" id="confirm-password" placeholder="Confirm password" />
            <div class="error"></div>
            <input type="submit" class="register-submit-button" value="Continue" data-ga-label="submit" />
        </form>
    </div>
</div>

<div id="login-modal" class="modal hidden" data-ga-action="login">
    <div class="modal-content"></div>
</div>

<div id="subscriptions-modal" class="modal hidden" data-ga-action="about subscriptions">
    <div class="modal-content">
        <h1>About Subscriptions</h1>
        <button class="close" data-ga-label="close"></button>
        <p class="description">Your Ingage Plus subscription will begin immediately upon purchase, after which you will be charged $14.99/month or $129.99/year on a recurring basis until you cancel.</p>
        <p class="description">To cancel at any time, email <a data-ga-label="email" href="mailto:ingage@scrollmotion.com">ingage@scrollmotion.com</a>. Subscription cancellations must be made at least 24 hours prior to the end of the current subscription period to avoid being charged for the next month or year. No refunds or credits will be giving for partial months.</p>
        <p class="description">By subscribing you agree to our <a data-ga-label="terms" href="<?php echo home_url(); ?>/terms">Terms of Use</a> and <a data-ga-label="privacy" href="<?php echo home_url(); ?>/privacy">Privacy Policy</a>.</p>
        <button class="continue-button button" data-ga-label="continue">Continue</button>
    </div>
</div>

<div id="purchase-modal" class="modal hidden" data-ga-action="purchase">
    <div class="modal-content"></div>
</div>

<div id="message-modal" class="modal hidden" data-ga-action="message">
    <div class="modal-content">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/smiley_face@2x.png">
        <h1></h1>
        <button data-ga-label="close" class="close"></button>
        <p class="description"><span class="message"></span><br>Now you can share your story with the world, then update it for all users with just a tap.<br><br>We’ve sent a confirmation email to <span class="email"></span>.</p>
        <a href="https://itunes.apple.com/us/app/ingage-interactive-presentations/id1100355870?mt=8" class="app-store-button button" data-ga-label="app store">Download Ingage</a>
        <a data-ga-label="upgrade" class="sign-in upgrade-link">Ready for Ingage Plus? Upgrade now.</a>
    </div>
</div>
