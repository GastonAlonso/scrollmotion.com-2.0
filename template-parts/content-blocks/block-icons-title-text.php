<?php
/**
 * Template for the Image "Icons Title Text" and "Video Icons Title Text" content block.
 * Identical except for their media type (video/image).
 */
?>

<?php
    $theme_class = '';

    if ( get_sub_field( 'media_type' ) === 'image'  ) :
        $theme_class = 'image';
    else :
        $theme_class = 'video';
    endif;
?>

<section class="icons-title-text icons-title-text--<?php echo $theme_class ?>">
    <?php if ( get_sub_field( 'media_type' ) == 'image' ) : ?>
        <img class='icons-title-text__image' src="<?php
            checkIfFieldValueIsIDAndReturnTheURLAndDeleteThisFunctionLaterLikeReallyDoIt(get_sub_field( 'image' ), 'large');
        ?>">
        <div class='icons-title-text__image-small' style="background-image: url('<?php
            checkIfFieldValueIsIDAndReturnTheURLAndDeleteThisFunctionLaterLikeReallyDoIt(get_sub_field( 'image_small' ), 'medium');
        ?>')"></div>
    <?php else : ?>
        <video class='icons-title-text__video video-focus' src="<?php the_sub_field( 'video' ) ?>" autoplay muted playsinline loop></video>
    <?php endif; ?>

    <?php if ( have_rows( 'icons') ) : ?>
        <div class='icons-title-text__icons'>
            <?php while ( have_rows( 'icons' ) ) : the_row(); ?>
                <img class='title-text-icons__icon' src="<?php
                    checkIfFieldValueIsIDAndReturnTheURLAndDeleteThisFunctionLaterLikeReallyDoIt(get_sub_field( 'icon' ), 'thumbnail');
                ?>">
            <?php endwhile; ?>
        </div>
    <?php endif; ?>

    <h2 class='title-text__title icons-title-text__title'><?php the_sub_field( 'title' ); ?></h2>
    <p class='title-text__text icons-title-text__text'>
        <?php the_sub_field( 'text' ) ?>
    </p>
</section>
