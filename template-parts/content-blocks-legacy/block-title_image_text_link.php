<?php
/**
 * Template for the Title Image Text Link legacy content block.
 */

// Get classes for the top and bottom margins
$classes     = get_sub_field('margins_and_gradient');
$classString = '';

if ( $classes ):
    foreach ( $classes as $class ):
        $classString.= $class;
        $classString.= ' ';
    endforeach;
endif;
?>

<section class="<?php echo $classString; ?>">
    <div class="section-content">
        <div class="title-content">
            <h1><?php echo the_sub_field( 'title' ); ?></h1>
        </div>

        <?php

        $image_spotlight = get_sub_field( 'image_spotlight' );

        if( $image_spotlight ):
            foreach( $image_spotlight as $spotlight ): ?>

            <div class="spot-content">
                <div class="image-content">
                    <?php echo wp_get_attachment_image( $spotlight[ 'image' ], 'full' ); ?>
                </div>
                <div class="text-content">
                    <p><?php echo $spotlight[ 'fname' ]; ?></p>
                    <p><?php echo $spotlight[ 'lname' ]; ?></p>
                </div>
                <div class="link-content">
                    <a href="<?php echo $spotlight[ 'button_url' ]; ?>">
                        <?php echo $spotlight[ 'button' ]; ?>
                    </a>
                </div>
            </div>

        <?php
            endforeach;
        endif;?>

    </div>
</section>
