<?php
/**
 * Template for the Hero legacy content block.
 */
?>

<?php if ( get_sub_field( 'background_image' ) ) : ?>

    <section class="hero" style="background-image: url('<?php the_sub_field( 'background_image' ) ?>')">

<?php else : ?>

    <section class="hero">

    <?php endif; ?>

        <div class="section-content no-padding">
            <div class="title-content">
                <h1><?php echo the_sub_field( 'title' ); ?></h1>
            </div>
            <div class="text-content">
                <p><?php echo the_sub_field( 'text' ); ?></p>
            </div>
        </div>

    <?php if ( get_sub_field( 'background_video' ) ) : ?>

        <video class="background-video" src="<?php echo the_sub_field( 'background_video' ); ?>" autoplay playsinline loop muted></video>
        <div class="background-scrim"></div>

    <?php endif; ?>

</section>
