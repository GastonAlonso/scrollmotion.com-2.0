<?php
/**
 * Template for the Image 2up Label Text legacy content block.
 */

// Get classes for the top and bottom margins
$classes     = get_sub_field('margins_and_gradient');
$classString = '';

if ( $classes ):
    foreach ( $classes as $class ):
        $classString.= $class;
        $classString.= ' ';
    endforeach;
endif;
?>

<section
    class="image-2up-label-text <?php echo $classString; ?>">
    <div class="section-content">
        <!-- Gallery - loop through array -->

        <?php

        $blocks = get_sub_field( 'blocks' );

        if( $blocks ):
            foreach( $blocks as $index=>$block ) : ?>

                <div class="two-up-content">
                    <div class="image-content">
                        <?php
                            if ( $block[ 'image_url' ] ) :
                                echo '<a target="_blank" href="' . $block[ 'image_url' ] . '">';
                            endif;

                            echo wp_get_attachment_image( $block[ 'image' ], 'full');

                            if ( $block[ 'image_url' ] ) :
                                echo '</a>';
                            endif;
                        ?>
                    </div>
                    <div class="label-content">
                        <h2 class="header2"><?php echo $block[ 'label' ]; ?></h2>
                    </div>
                    <div class="text-content">
                        <p><?php echo $block[ 'text' ]; ?></p>
                    </div>
                </div>

        <?php
            endforeach;

        endif;?>

    </div>
</section>
