<?php
/**
 * Template for the Support page legacy content block.
 */
?>

<section class="support-page">
    <div class="section-content">
        <div class="title-content">
            <h1><?php the_sub_field( 'title' ); ?></h1>
            <h2><?php the_sub_field( 'subtitle' ); ?></h2>
        </div>

        <?php

            if( have_rows('blocks') ) :
                $i = 1;

                // loop through the rows of data
                while ( have_rows('blocks') ) : the_row(); ?>

                    <div class="support-funnel">
                        <a href="<?php the_sub_field( 'button_url' ); ?>">
                            <div class="image-content">
                                <?php echo wp_get_attachment_image( get_sub_field( 'image' ), 'full' ); ?>
                            </div>
                            <div class="text-content">
                                <p><?php the_sub_field( 'text' ); ?></p>
                            </div>
                            <div class="link-content">
                                <?php the_sub_field( 'button_text' ); ?>
                            </div>
                        </a>
                    </div>

                <?php
                $i++;

                endwhile;

            endif; ?>

    </div>
</section>
