<?php
/**
 * The template for displaying blog posts
 *
 * Blog is a custom post category, default posts are used by press releases.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */

get_header(); ?>

    <section class="blog" data-ga-category="blog">
        <div class="section-content">
            <div class="title-content">
                <h1><?php single_post_title(); ?></h1>
            </div>
            <?php
                $current_page = get_query_var('paged') ? get_query_var('paged') : 1;
                $args = array(
                    'post_type'      => 'blog-posts',
                    'posts_per_page' => get_option( 'posts_per_page' ),
                    'paged'          => $current_page
                );
                $blog_posts = new WP_Query( $args );

                if ( $blog_posts->have_posts() ) :

                    // Start the loop.
                    while ( $blog_posts->have_posts() ) : $blog_posts->the_post();

                        get_template_part( 'template-parts/blog/content', get_post_format() );

                    // End the loop.
                    endwhile;

                    // Numeric page navigation.
                    echo paginate_links( array(
                        'current'   => $current_page,
                        'total'     => $blog_posts->max_num_pages,
                        'prev_next' => false,
                        'type'      => 'list'
                    ) );

                    // reset original post data
                    wp_reset_postdata();

                // If no content, include the "No posts found" template.
                else :

                    get_template_part( 'content', 'none' );

                endif;
            ?>
        </div>
    </section>

<?php get_footer();
