<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 */

get_header(); ?>

<section class="not-found">
    <div class="section-content">
        <div class="title-content xxl">
            <h1>404</h1>
        </div>
        <div class="text-content">
            <p>
                Sorry the page you’re looking for can’t be found. Maybe it moved, or is out to lunch. I dunno, but its definitely not here.
            </p>
        </div>
    </div>
</section>

<?php get_footer();