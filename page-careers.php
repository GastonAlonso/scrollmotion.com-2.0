<?php
/**
 * Custom PHP script that pulls job listings from JobVite.
 */

get_header(); ?>

<section class="careers" data-ga-category="careers">
    <div class="section-content">
        <div class="title-content">
            <h1><?php the_title(); ?></h1>
            <div class="bebop-jobs"></div>
            <script id="bebop-embed-loader" async defer src="https://hire.withgoogle.com/s/embed/jobs.js?company=scrollmotioncom"></script>
        </div>
    </div>
</section>

<?php get_footer(); ?>
