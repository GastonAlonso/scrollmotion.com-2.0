<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 */

get_header(); ?>

    <section class="article-page">
        <div class="section-content">

        <?php
            /* Start the loop. */
            while ( have_posts() ) : the_post();

                get_template_part( 'template-parts/post/single/content', get_post_format() );

            endwhile; // End the loop.
        ?>

        </div>
    </section>

<?php get_footer();
