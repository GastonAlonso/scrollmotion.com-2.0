<!DOCTYPE html>
<html>
    <head>
        <?php if (preg_match('~MSIE|Internet Explorer~i', $_SERVER['HTTP_USER_AGENT']) || (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') !== false)) { ?>
        <script >window.location = '/update-browser';</script>
        <?php } ?>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <?php include_once( 'inc/env-cfg.php' ); ?>
        <?php include_once( 'inc/js-ns.php' ); ?>
        <?php wp_head(); ?>
    </head>
    <body>
        <div id="app"></div>

        <script>
            (function () {
                var config = window.dashboardConfig || {};

                config.landingUrl = window.scrollmotion.homeUrl + '/real-estate-dashboard';
                config.apiUrl     = window.scrollmotion.apiUrl;
                config.clientId   = window.scrollmotion.clientId;
                config.stripeKey  = window.scrollmotion.stripePublicKey;

                window.dashboardConfig = config;
            })();
        </script>
        <script src="<?php echo get_template_directory_uri() . '/real-estate-dashboard/build.js' ?>"></script>

        <?php wp_footer(); ?>
    </body>
</html>
